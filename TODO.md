found bugs
==========
* player and god dirs are not available, but engine doesn't complain
* notes are not saved
*  BUG: Could not open one of the note files in append mode
* no check for rundir


need to implement
=================

* noautotitle
* as a general, need more auto options per char
* custom recall per char
* portal objects
* update hp, mana and moves every sec (not by tick)
* global counters
** xp by everyone for a period of time
** xp per player for last N periods
** how good you are in XP gain comparing to others
* DONE create more items by casting 'create food'
* DONE last command
** DONE last logged in
** last events login-logout-levelup

good to be done
===============

* extra objects, rooms
* experimental dwarvish race with autotranslation to runes

each feature should be documented
=================================

* last
* mem
* map
* show

general ideas
=============

* race homes, elvish in quenya, dwarfs in runes, mages in esperanto
* transport dragons and mages, first is cheaper but longer and can
  drain some hp, second option is safer and creates portals
* smelling large animals; you smell the russian odour from the east

Channels
========
Shout  (restrict freq, no more than 1 shout per char per minute)
Gossip (restrict lev, no gossips while you're small)
Newbie (restrict write to newbies, answer them directly)

Yell (to area)
Say  (to room)
Tell (to char, in private)

m.b. more race-only, class-only, group-only, guild-only channels?

Commands
========
* *mem*  should show the memory usage, including dynamic change in time,
         like how many objects were added since startup, or last 5 mins,
         plus the ability to show objects related to certain area/room
* *map*  can draw maps for the current location, in different form,
         available for different races/classes/spells/skills
* *last* last [month/week/day/hour] xp/login
* *show* show mem/cpu/xp/obj/mob/npc/pc month/week/day/hour dump/graph

