/*
 * log.c
 *
 * Copyright 2017 Serge Ribalchenko <fisher@heim.in.ua>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include <stdio.h>
#include <stdarg.h>
#include <time.h>
#include "merc.h"

/*
 * private functions
 */
void     log_string          args( ( const char *str ) );

/*
 * this should be the only exported function from here
 */
void logit (char *fmt, ...)
{
    char buf[2 * MSL];
    va_list args;
    va_start (args, fmt);
    vsprintf (buf, fmt, args);
    va_end (args);

    log_string (buf);
}

/*
 * Writes a string to the log.
 */
void log_string (const char *str)
{
    struct tm *stm;

    stm = localtime(&current_time);
    fprintf (stderr, "%02d/%02d/%02d %02d:%02d:%02d  %s\n",
            stm->tm_year -100, stm->tm_mon +1, stm->tm_mday,
            stm->tm_hour, stm->tm_min, stm->tm_sec, str);
    return;
}
