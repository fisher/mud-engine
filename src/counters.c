/*
 * counters.c
 *
 * Copyright 2016 Serge Rybalchenko <fisher@heim.in.ua>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <stdio.h>
#include <sys/time.h>
#include <string.h>
#include "merc.h"           /* for game's time_info */

/* local for this module */
int cnt_xp_day[24] = {0};
int cnt_xp_week[7] = {0};

int cnt_mem_hour[60] = {0};
int cnt_mem_day[24] = {0};
int cnt_mem_month[35] = {0};

// print buffer, resides here, can be exported
//static char cnt_buf[120*40+1] = {0};

void init_counters()
{
    // don't need it to initialize it that way because of ={0} definition
    for (int i=0; i<60; i++) cnt_mem_hour[i]=0;
    return;
}

void cnt_update_minute(int minute)
{
    cnt_mem_hour[minute] = 0;
    return;
}

void cnt_update_hour(int hour)
{
    cnt_mem_day[hour] = 0;
    return;
}

void cnt_update_day(int day)
{
    cnt_mem_month[day] = 0;
    return;
}

void counter_mem(int amount)
{
    /*
    logit("cnt_mem[%d] <- %d + %d = %d", time_info.minute,
        cnt_mem_hour[time_info.minute], amount,
        cnt_mem_hour[time_info.minute] + amount);
    */
    cnt_mem_hour[time_info.minute] += amount;
    cnt_mem_day[time_info.hour] += amount;
    cnt_mem_month[time_info.day] += amount;
    return;
}

int counter_mem_last_hour()
{
    int result = 0;
    for (int i=0; i<60; i++) result += cnt_mem_hour[i];
    return result;
}

int counter_mem_last_day()
{
    int result = 0;
    for (int i=0; i<24; i++) result += cnt_mem_day[i];
    return result;
}

int counter_mem_last_month()
{
    int result = 0;
    for (int i=0; i<35; i++) result += cnt_mem_month[i];
    return result;
}

/*
 * show mem/cpu/xp/obj/mob/npc/pc (last) month/day/hour dump/graph
 */

void counter_mem_graph(CHAR_DATA *ch)
{
    char buf[MAX_STRING_LENGTH] = {0};

    for (int i=0; i< 24; i+=6)
        sprintf (buf + strlen(buf), "%6d %6d %6d    %6d %6d %6d \r\n",
                cnt_mem_day[i], cnt_mem_day[i+1], cnt_mem_day[i+2],
                cnt_mem_day[i+3], cnt_mem_day[i+4], cnt_mem_day[i+5]);

    send_to_char (buf,ch);
    return;
}

void counter_xp_increase(int xp)
{
    cnt_xp_day[time_info.hour]++;
    cnt_xp_week[time_info.day]++;
    return;
}

int counter_xp_last_day()
{
    int result = 0;
    int i;
    for (i=0; i<24; i++) result += cnt_xp_day[i];
    return result;
}

int counter_xp_last_week()
{
    int result = 0;
    int i;
    for (i=0; i<7; i++) result += cnt_xp_week[i];
    return result;
}

int counter_xp_last_month()
{
    return 0;
}

void counter_levelup_increment()
{
    return;
}

char last_logins[10][100];
static int last_logins_idx = 0;

void update_last_login(CHAR_DATA *ch) {
    sprintf(last_logins[last_logins_idx++], "%s", ch->name);
    if (last_logins_idx >9) last_logins_idx = 0;
}

void do_last(CHAR_DATA *ch, char *argument) {
    char buf[MAX_STRING_LENGTH];
    buf[0] = 0;
    for (int i=last_logins_idx; i<10; i++)
        if (last_logins[i][0] != 0)
            sprintf(buf + strlen(buf), "{W%s{x logged\r\n", (char *)last_logins[i]);
    for (int i=0; i<last_logins_idx; i++)
        sprintf(buf + strlen(buf), "{W%s{x logged\r\n", (char *)last_logins[i]);
    send_to_char("Last logged in players:\r\n", ch);
    send_to_char(buf, ch);
}

