#include "mud.h"

/*
 * Using switch to avoid surprises with indicies
 */
const char *exit_string(enum Direction direction) {
    switch (direction) {
    case DIR_NORTH:
        return "North";
    case DIR_EAST:
        return "East";
    case DIR_SOUTH:
        return "South";
    case DIR_WEST:
        return "West";
    case DIR_UP:
        return "Up";
    case DIR_DOWN:
        return "Down";
    default:
        return "Unknown direction";
    };
}
