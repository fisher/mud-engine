/***************************************************************************
 *  Original Diku Mud copyright (C) 1990, 1991 by Sebastian Hammer,        *
 *  Michael Seifert, Hans Henrik Strfeldt, Tom Madsen, and Katja Nyboe.    *
 *                                                                         *
 *  Merc Diku Mud improvments copyright (C) 1992, 1993 by Michael          *
 *  Chastain, Michael Quan, and Mitchell Tse.                              *
 *                                                                         *
 *  In order to use any part of this Merc Diku Mud, you must comply with   *
 *  both the original Diku license in 'license.doc' as well the Merc       *
 *  license in 'license.txt'.  In particular, you may not remove either of *
 *  these copyright notices.                                               *
 *                                                                         *
 *  Much time and thought has gone into this software and you are          *
 *  benefitting.  We hope that you share your changes too.  What goes      *
 *  around, comes around.                                                  *
 ***************************************************************************/

/***************************************************************************
 *  ROM 2.4 is copyright 1993-1998 Russ Taylor                             *
 *  ROM has been brought to you by the ROM consortium                      *
 *      Russ Taylor (rtaylor@hypercube.org)                                *
 *      Gabrielle Taylor (gtaylor@hypercube.org)                           *
 *      Brian Moore (zump@rom.org)                                         *
 *  By using this code, you have agreed to follow the terms of the         *
 *  ROM license, in the file Rom24/doc/rom.license                         *
 ***************************************************************************/

#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "merc.h"
#include "mud.h"

char *const distance[4] = {
    "right here.", "nearby to the %s.", "not far %s.",
    "off in the distance %s."
};

void scan_list args ((ROOM_INDEX_DATA * scan_room, CHAR_DATA * ch,
                      sh_int depth, sh_int door));
void scan_char args ((CHAR_DATA * victim, CHAR_DATA * ch,
                      sh_int depth, sh_int door));
void do_scan (CHAR_DATA * ch, char *argument)
{
    extern char *const dir_name[];
    char arg1[MAX_INPUT_LENGTH], buf[MAX_INPUT_LENGTH];
    ROOM_INDEX_DATA *scan_room;
    EXIT_DATA *pExit;
    sh_int door, depth;

    argument = one_argument (argument, arg1);

    if (arg1[0] == '\0')
    {
        act ("$n looks all around.", ch, NULL, NULL, TO_ROOM);
        send_to_char ("Looking around you see:\r\n", ch);
        scan_list (ch->in_room, ch, 0, -1);

        for (door = 0; door < 6; door++)
        {
            if ((pExit = ch->in_room->exit[door]) != NULL)
                scan_list (pExit->u1.to_room, ch, 1, door);
        }
        return;
    }
    else if (!str_cmp (arg1, "n") || !str_cmp (arg1, "north"))
        door = 0;
    else if (!str_cmp (arg1, "e") || !str_cmp (arg1, "east"))
        door = 1;
    else if (!str_cmp (arg1, "s") || !str_cmp (arg1, "south"))
        door = 2;
    else if (!str_cmp (arg1, "w") || !str_cmp (arg1, "west"))
        door = 3;
    else if (!str_cmp (arg1, "u") || !str_cmp (arg1, "up"))
        door = 4;
    else if (!str_cmp (arg1, "d") || !str_cmp (arg1, "down"))
        door = 5;
    else
    {
        send_to_char ("Which way do you want to scan?\r\n", ch);
        return;
    }

    act ("You peer intently $T.", ch, NULL, dir_name[door], TO_CHAR);
    act ("$n peers intently $T.", ch, NULL, dir_name[door], TO_ROOM);
    sprintf (buf, "Looking %s you see:\r\n", dir_name[door]);

    scan_room = ch->in_room;

    for (depth = 1; depth < 4; depth++)
    {
        if ((pExit = scan_room->exit[door]) != NULL)
        {
            scan_room = pExit->u1.to_room;
            scan_list (pExit->u1.to_room, ch, depth, door);
        }
    }
    return;
}

void scan_list (ROOM_INDEX_DATA * scan_room, CHAR_DATA * ch, sh_int depth,
                sh_int door)
{
    CHAR_DATA *rch;

    if (scan_room == NULL)
        return;
    for (rch = scan_room->people; rch != NULL; rch = rch->next_in_room)
    {
        if (rch == ch)
            continue;
        if (!IS_NPC (rch) && rch->invis_level > get_trust (ch))
            continue;
        if (can_see (ch, rch))
            scan_char (rch, ch, depth, door);
    }
    return;
}

void scan_char (CHAR_DATA * victim, CHAR_DATA * ch, sh_int depth, sh_int door)
{
    extern char *const dir_name[];
    extern char *const distance[];
    char buf[MAX_INPUT_LENGTH], buf2[MAX_INPUT_LENGTH];

    buf[0] = '\0';

    strcat (buf, PERS (victim, ch));
    strcat (buf, ", ");
    sprintf (buf2, distance[depth], dir_name[door]);
    strcat (buf, buf2);
    strcat (buf, "\r\n");

    send_to_char (buf, ch);
    return;
}

int depth;
static char map[11][16] = {
    "+-+-+-v-+-+-+\r\n",
    "|     o     |\r\n",
    "|     |     |\r\n",
    "+     o     +\r\n",
    "|     |     |\r\n",
    ">-o-o-X-o-o-<\r\n",
    "|     |     |\r\n",
    "+     o     +\r\n",
    "|     |     |\r\n",
    "|     o     |\r\n",
    "+-+-+-^-+-+-+\r\n" };
char esym;

int opposite_exit(int direction) {
    switch (direction) {
    case 0:
        return 2; /* n -> s */
    case 1:
        return 3; /* e -> w */
    case 2:
        return 0; /* s -> n */
    case 3:
        return 1; /* w -> e */
    case 4:
        return 5; /* u -> d */
    case 5:
        return 4; /* d -> u */
    default:
        return -1;
    };
}

void mark_exit(int door, int currx, int curry, char symb)
{
    switch (door) {
    case 0:
        map[curry-1][currx] = symb; break;
    case 1:
        map[curry][currx+1] = symb; break;
    case 2:
        map[curry+1][currx] = symb; break;
    case 3:
        map[curry][currx-1] = symb;
    };
}

void traverse_exits (CHAR_DATA *ch, ROOM_INDEX_DATA *room, sh_int curr_vnum,
                     int entrance, int currx, int curry, int dbg)
{
    sh_int door;
    EXIT_DATA *pExit;
    ROOM_INDEX_DATA *next_room;
    char buf[MAX_INPUT_LENGTH];
    const char *CDir;
    char rsym;
    int comeback, next_vnum, dx = 0, dy = 0;

    depth++;
    if (depth >2) {if(dbg) send_to_char("max depth reached\r\n", ch); return;}

    /* check 2d boundaries */
    if (currx > 9 || currx < 3) {
        if(dbg) {
        sprintf(buf, "(%d,%d) X boundary reached\r\n", currx, curry);
        send_to_char(buf, ch); } return;}
    if (curry > 8 || curry < 2) {
        if(dbg) {
        sprintf(buf, "(%d,%d) Y boundary reached\r\n", currx, curry);
        send_to_char(buf, ch); } return;}

    for (door = 0; door < 4; door++) {
        /* if this is the door to where we've come from, skip it */
        if (door == entrance) {
            if(dbg) {
            sprintf(buf, "%s is the entrance to current %d (%d,%d)\r\n",
                    exit_string(door), curr_vnum, currx, curry);
            send_to_char(buf, ch); }
            continue;
        }
        if ((pExit = room->exit[door]) != NULL) {
            next_room = pExit->u1.to_room;
            if( next_room == NULL ) {
                mark_exit(door, currx, curry, '#');
                logit("bug in room exit %d:%d -> NULL",
                room->vnum, door);
                continue;
            }
            next_vnum = next_room->vnum;
            CDir = exit_string(door);
            comeback = opposite_exit(door);

            if(dbg) {
                sprintf(buf, "(%d,%d) %d %s exit: %d (%c)\r\n",
                        currx, curry, curr_vnum, CDir, next_vnum, esym);
                send_to_char(buf, ch);
            }

            //if the entrance doesn't point to origin vnum, skip it
            if (next_room->exit[comeback] == NULL) {
                mark_exit(door, currx, curry, '%');
                if(dbg)
                    send_to_char("opposite exit points to NULL\r\n", ch);
                continue;
            }
            if (next_room->exit[comeback]->u1.to_room->vnum != curr_vnum) {
                mark_exit(door, currx, curry, '*');
                if(dbg)
                    send_to_char("opposite exit does not return us back\r\n", ch);
                continue;
            }

            // mark up/down exits
            if (next_room->exit[4] != NULL && next_room->exit[5] != NULL) {
                rsym = '/';
            } else if (next_room->exit[4] != NULL) {
                rsym = '^';
            } else if (next_room->exit[5] != NULL) {
                rsym = 'v';
            } else rsym = 'o';

            // put a symbol on map
            switch (door) {
            case 0:
                map[curry-1][currx] = pExit->exit_info?'+':'|';
                map[curry-2][currx] = dbg?esym++:rsym;
                dx = 0; dy = -2; break;
            case 1:
                map[curry][currx+1] = pExit->exit_info?'+':'-';
                map[curry][currx+2] = dbg?esym++:rsym;
                dx = 2; dy = 0; break;
            case 2:
                map[curry+1][currx] = pExit->exit_info?'+':'|';
                map[curry+2][currx] = dbg?esym++:rsym;
                dx = 0; dy = 2; break;
            case 3:
                map[curry][currx-1] = pExit->exit_info?'+':'-';
                map[curry][currx-2] = dbg?esym++:rsym;
                dx = -2; dy = 0;
            };

            // recursively call that exit
            traverse_exits(ch, next_room, next_vnum, comeback, currx+dx, curry+dy, dbg);
            depth--;
        }
    }
}

void traverse_neighbours(CHAR_DATA *ch, int d, char strs[d*2+3][d*2+6], int rms[d][d], int x, int y, int ini_dir)
{
    //char buf[MAX_INPUT_LENGTH];

    // [row][column]
    strs[x*2+2][y*2+2] = '!';
    rms[d/2][d/2] = 9;

    return;
}


void map_compact_init(int dim, char map[dim*2+3][dim*2+6], int rooms[dim][dim]);

void map_compact5(CHAR_DATA *ch)
{
    static int rooms[5][5];

    /* dim *2 +1 (opposite side) +2 (borders) +3 (\r\n\0) */
    static char strmap[13][16];

    map_compact_init(5, strmap, rooms);
    traverse_neighbours(ch, 5, strmap, rooms, 2, 2, 0);

    for (register int i = 0; i< 13; i++) {
        printf_to_char(ch, "{x*==%02d==> {c", i);
        send_to_char(strmap[i], ch); }
    send_to_char("{x<==\r\n", ch);

    for (int i = 0; i<5; i++) {
        for (int j = 0; j<5; j++) {
            printf_to_char(ch, "r[%d][%d]=%d ", i, j, rooms[i][j]);
        }
        send_to_char("\r\n", ch);
    }
}

void map_simple(CHAR_DATA *ch)
{
    int i;

    strcpy(map[0], "+-----v-----+\r\n");
    for (i=1; i<10; i++) strcpy(map[i], "|           |\r\n");
    strcpy(map[10], "+-----^-----+\r\n");

    depth = 0; esym = 'a';
    map[5][6] = 'X'; map[5][0] = '>'; map[5][12] = '<';
    traverse_exits(ch, ch->in_room, ch->in_room->vnum, -1, 6, 5, 0);

    for (i = 0; i< 11; i++)
        send_to_char(map[i], ch);
}

void do_map (CHAR_DATA * ch, char *argument)
{
    if (argument[0] != 0) {
        if (!str_prefix(argument, "compact")) {
            map_compact5(ch);
        } else if (!str_prefix(argument, "simple")) {
            map_simple(ch);
        } else {
            map_simple(ch);
        }
    } else {
        map_simple(ch);
    }

    return;
}


void do_sss (CHAR_DATA * ch, char *argument)
{
    int i;
    ROOM_INDEX_DATA *start_room;
    char buf[MAX_INPUT_LENGTH];

    strcpy(map[0], "+-----v-----+\r\n");
    for (i=1; i<10; i++) strcpy(map[i], "|           |\r\n");
    strcpy(map[10], "+-----^-----+\r\n");

    start_room = ch->in_room;

    sprintf(buf, "Inroom: %d\r\n", start_room->vnum);
    send_to_char(buf, ch);

    depth = 0; esym = 'a';
    map[5][6] = 'X'; map[5][0] = '>'; map[5][12] = '<';
    traverse_exits(ch, ch->in_room, start_room->vnum, -1, 6, 5, 1);

    for (i = 0; i< 11; i++)
        send_to_char(map[i], ch);

    return;
}

/* init the arrays of chars and rooms */
void map_compact_init(int dim, char strmap[dim*2+3][dim*2+6], int rooms[dim][dim])
{
    register int i,j;
    int hmax = dim*2+2;

    for (i=0; i<dim; i++) for (j=0; j<dim; j++) rooms[i][j] = 3;

    for (i=1; i< dim*2+2; i++) {
        strmap[i][0] = '|';
        for (j=1; j< dim*2+2; j++) strmap[i][j]=32;
        strmap[i][j++] = '|'; strmap[i][j++] = 13;
        strmap[i][j++] = 10;  strmap[i][j] = 0;
    }

    for (j=1; j< hmax; j++) {
        strmap[0][j] = '-';
        strmap[hmax][j] = '-';
    }

    strmap[0][j] = '+';    strmap[0][0] = '+';
    strmap[j][j] = '+';    strmap[j][0] = '+';

    strmap[dim+1][0] = '>';
    strmap[dim+1][j] = '<';
    strmap[0][dim+1] = 'v';
    strmap[j][dim+1] = '^';

    strmap[0][j+1] = 13; strmap[j][j+1] = 13;
    strmap[0][j+2] = 10; strmap[j][j+2] = 10;
    strmap[0][j+3] = 0;  strmap[j][j+3] = 0;
}
