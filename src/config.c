/*
 * config.c
 *
 * Copyright 2019 Serge Rybalchenko <fisher@heim.in.ua>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libgen.h>
#include <unistd.h>
#include <getopt.h>
#include <netdb.h>
#include <errno.h>

//#include "engine.h"
#include "log.h"
#include "version.h"

#define CONFIG_C
#include "config.h"
#undef CONFIG_C

#define BUFSIZE 1024

volatile ENGINE_CFG_T cfg =
    {
        /* defaults */
        .verbose = 1,
        .daemonize = 1,
        .logtype = 0,
        .logfile = "engine.log",
        .logdir = "log/",
        .userdir = "player/",
        .gods = "gods/",
        .world = "world/",
        .port = 3000,
        .hostname = "localhost",
        .fqdn = "localhost.localdomain",
        .ip = "0.0.0.0",
        .rundir = "default-rundir",
        .pidfile = "engine.pid",
        .exe_file = NULL
    };

void usage(char *self) {
    printf("Usage:\t%s [options]\r\n"
           "\r\n"
           "\t--version\t\tprint version and exit\r\n"
           "\t--help\t\t\tprint this help and exit\r\n"
           "\t--verbose\t\tset verbosity level to 2 (info)\r\n"
           "\t--quiet\t\t\tset verbosity level to 0 (errors only)\r\n"
           "\t--hostname <hostname>\tset the FQDN (default: %s)\r\n"
           "\t--nodetach\t\tdon't go background\r\n"
           "\t--rundir <directory>\tset working directory\r\n"
           "\t--pidfile <filename>\tfor detach only (default: %s)\r\n"
           "\t--ip <ip_address>\tset the IP to be bound to\r\n"
           "\t--port <num>\t\tport number to listen (default: %d)\r\n"
           "\t--setcookie <cookie>\tset erlang cookie\r\n"
           "\t--getcookie <filename>\tget the cookie from file\r\n"
           "\r\n"
           , basename(self), cfg.fqdn, cfg.pidfile, cfg.port);
}

void print_version()
{
    printf("Asylum engine v%s \"%s\", source revision/build: %s\r\n",
           VERSION, RELEASE, REVISION);
}

char *restore_state(char *filename)
{
    FILE *file;
    char buf[128];
    char *cookie;

    if(NULL == (file = fopen(filename, "r"))) {
        fprintf(stderr, "error reading file %s: %s\r\n",
                filename, strerror(errno));
        exit(255);
    }

    if(NULL == fgets(buf, sizeof(buf), file)) {
        fprintf(stderr,
                "error reading file %s: fgets() returns NULL\r\n",
                filename);
        exit(255);
    }

    fclose(file);

    buf[strcspn(buf, "\r\n")] = 0;

    cookie = (char *) malloc(strlen(buf));

    strcpy (cookie, buf);

    return cookie;
}

void cfg_init(int argc, char **argv) {

    int c;
    char buf[BUFSIZE];
    struct hostent *h;
    extern int control;

    static int verbose_flag, daemonize, help_flag, flush_flag;

    cfg.exe_file = argv[0];

    for (int i = 0; i< argc; i++) printf("argv[%d]: %s\n", i, argv[i]);

    /* we need to know our hostname */
    if (gethostname(buf, BUFSIZE)) {
        perror("gethostname() error");
    } else {
        cfg.hostname = (char *) malloc( strlen(buf) +1);
        strcpy(cfg.hostname, buf);
    }

    /* and FQDN too */
    h = gethostbyname(cfg.hostname);
    if (h == NULL) {
        fprintf(stderr, "gethostbyname() returns error, going to use defaults\r\n");
    } else {
        cfg.fqdn = h->h_name;
    }

    verbose_flag=cfg.verbose, daemonize=cfg.daemonize;
    help_flag = 0;

    struct option long_options[] =
        {
            {"verbose",   no_argument, &verbose_flag, 2},
            {"quiet",     no_argument, &verbose_flag, 0},
            {"debug",     no_argument, &verbose_flag, 3},
            {"nodetach",  no_argument, &daemonize,    0},
            {"detached",  no_argument, &daemonize,    1},
            {"help",      no_argument, &help_flag,    1},
            {"flush",     no_argument, &flush_flag,   1},
            {"version",   no_argument,       0, 'b'},
            {"port",      required_argument, 0, 'p'},
            {"hostname",  required_argument, 0, 's'},
            {"ip",        required_argument, 0, 'i'},
            {"name",      required_argument, 0, 'n'},
            {"rundir",    required_argument, 0, 'r'},
            {"pidfile",   required_argument, 0, 'f'},
            {"copyover",  required_argument, 0, 'c'},
            {0,0,0,0}
        };

    int option_index = 0;

    /* now let's parse command-line arguments */
    while( (c = getopt_long(argc, argv, "v:dp:s:r:i:f:",
                            long_options, &option_index)) != -1)
        switch (c) {
        case 0:
            if (long_options[option_index].flag != 0) {
                break;
            }
            printf("option %s", long_options[option_index].name);
            if (optarg)
                printf(" with arg %s", optarg);
            printf("\r\n");
            break;
        case 'v':
            cfg.verbose = atoi(optarg);
            break;
        case 'b':
            print_version();
            exit(0);
        case 'd':
            daemonize = 1;
            break;
        case 'p':
            cfg.port = atoi(optarg);
            break;
        case 's':
            cfg.fqdn = optarg;
            break;
        case 'r':
            cfg.rundir = optarg;
            break;
        case 'i':
            cfg.ip = optarg;
            break;
        case 'f':
            cfg.pidfile = optarg;
            break;
        case 'c':
            control = atoi(optarg);
            break;
        default:
            usage(argv[0]);
            exit(1);
        }

    /* merge defaults, config files, and command-line options here */
    cfg.daemonize = daemonize;
    cfg.verbose = verbose_flag;

    if (cfg.port <1024) {
            printf("Error: port number should be greater than 1024");
            exit(1);
    }
    if (help_flag) { usage(argv[0]); exit (0); }

    return;
}

FILE *open_file( const char* base, const char* name,
                 const char* mode, const char* error_msg )
{
    FILE *result;
    char fn_full[1024];

    strcpy(fn_full, base);
    strcat(fn_full, name);

    if ((result = fopen (fn_full, mode)) == NULL)
    {
        if (error_msg != NULL) logit("[*****] BUG: %s", error_msg);
        logit("[open_file, %s] %s: %s", mode, fn_full, strerror(errno));
        exit (1);
    }

    return result;
}


/*
 * Local Variables:
 * indent-tabs-mode: nil
 * show-trailing-whitespace: t
 * mode: c
 * End:
 *
 */
