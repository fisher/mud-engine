const char *format_position[] =
    {
        "You are {RDEAD{x!!\r\n",
        "You are mortally wounded.\r\n",
        "You are incapacitated.\r\n",
        "You are stunned.\r\n",
        "You are sleeping.\r\n",
        "You are resting.\r\n",
        "You are sitting.\r\n",
        "You are fighting.\r\n",
        "You are standing.\r\n"
    };
