enum Direction {DIR_NORTH, DIR_EAST, DIR_SOUTH, DIR_WEST, DIR_UP, DIR_DOWN};

const char *exit_string(enum Direction);
