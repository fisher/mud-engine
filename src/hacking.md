prelude
=======

* ===> comm.c:main()

** -------> config.c:cfg_init()

Fills up the global `cfg` struct

** -------> act_wiz.c:qmconfig_read()

*** fopen("qmconfig.rc","r");
*** sets 3 config variables
**** mud_ansicolor
**** mud_ansiprompt
**** mud_telnetga

TODO: functionality should be moved to config.c

right now it doesn't fail, like, at all

** -------> db.c:boot_db(

*** sets the rng

*** sets the weather and game time

*** loads all the area files

[...]

** -------> if(copyover) `act_wiz.c:copyover_recover()`

** -------> start main cycle `comm.c:game_loop_unix()`

main cycle
==========


key data structures
===================

* cfg
  Holds all the configuration options, defined in `config.h:engine_cfg` struct.

  Declared in config.h as `extern volatile ENGINE_CFG_T cfg`


important codepoints
====================

main(): [comm.c](comm.c)


copyover feature
================

start copyover
--------------

act_wiz.c:do_copyover()

does the trick; creates file with copyover data

```
#define COPYOVER_FILE "copyover.data"
#define EXE_FILE      "bin/rom"
```

then `exec1(EXE_FILE, "rom", "--copyover", buf2)`

catch copyover
--------------
comm.c:main()

after call to cfg_init() we're getting `--copyover` arg, reading world, and calling

`act_wiz.c:copyover_recover()'

used symbols
============

```
0 (* ^rom24:fisher) sofa% grep defined ./*[ch] |perl -ne 's/^.*defined\((.*)\).*$/$1/ && print "Symbol: ".$_' |sort -u
```

Before removing IMC
-------------------

Symbol: _AIX
Symbol: apollo
Symbol: const
Symbol: _DISKIO_H_
Symbol: FALSE
Symbol: FNDELAY
Symbol: __FreeBSD__
Symbol: hpux
Symbol: __hpux
Symbol: htons
Symbol: IMC1STMUD
Symbol: IMCACK
Symbol: IMCCHRONICLES
Symbol: IMCCIRCLE
Symbol: IMCMERC
Symbol: IMCROM
Symbol: IMCSMAUG
Symbol: IMCSTANDALONE
Symbol: IMCUENVY
Symbol: interactive
Symbol: IPV6
Symbol: isascii
Symbol: KEY
Symbol: KEYS
Symbol: linux
Symbol: macintosh
Symbol: MALLOC_DEBUG
Symbol: MIPS_OS
Symbol: MSDOS
Symbol: __NetBSD__
Symbol: NeXT
Symbol: NOCRYPT
Symbol: ntohl
Symbol: OLD_RAND
Symbol: QMFIXES
Symbol: sequent
Symbol: SMAUGSOCIAL
Symbol: social_debug
Symbol: sun
Symbol: __SVR4
Symbol: SYSV
Symbol: TRADITIONAL
Symbol: TRUE
Symbol: ultrix
Symbol: unix
Symbol:  VERBOSE

After removing IMC
------------------

Symbol: FALSE
Symbol: FNDELAY
Symbol: KEY
Symbol: KEYS
Symbol: linux
Symbol: MALLOC_DEBUG
Symbol: __NetBSD__
Symbol: NOCRYPT
Symbol: OLD_RAND
Symbol: QMFIXES
Symbol: social_debug
Symbol: SYSV
Symbol: TRUE
Symbol: __unix__
Symbol:  VERBOSE
