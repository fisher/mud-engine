/*
 * counters.h
 *
 * Copyright 2016 Serge Rybalchenko <fisher@heim.in.ua>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef COUNTERS_H
#define COUNTERS_H

void init_counters();
void cnt_update_minute();
void cnt_update_hour();
void cnt_update_day();

void counter_mem(int);
int counter_mem_last_hour(void);
int counter_mem_last_day(void);
int counter_mem_last_month(void);
void counter_mem_graph(CHAR_DATA *);

void counter_xp_increase(int xp);
int counter_xp_last_day(void);
int counter_xp_last_week(void);
int counter_xp_last_month(void);

void counter_levelup_increment(void);

void update_last_login(CHAR_DATA*) ;

#endif
