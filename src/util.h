void create_passwd(char *plain, char hash_string[62]);
int check_passwd(char *plain, char *hash_string);
void binary_to_string(char *string, char *binary, size_t size);
bool is_integer(char *);
