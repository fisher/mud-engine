#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>

#include "log.h"
#include "sha-256.h"
#include "base64.h"

/* helper function just to see the content of binary in hex string */
static void binary_to_string(char *string, char *binary, size_t size)
{
    size_t i;
    for (i = 0; i < size; i++) {
        string += sprintf(string, "%02x", binary[i]);
    }
}

bool is_integer(char *argument) {
    if ((*argument) == 0) return false;
    while (*argument != 0) {
        if (*argument < '0' || *argument > '9') return false;
        argument++;
    }
    return true;
}

void create_passwd(char *plain, char hash_string[62]) {
    uint8_t hash[32];
    size_t plain_size;
    unsigned char *plain_with_salt;

    plain_size = strlen(plain);

    /* placeholder for random seed (aka salt) + plain password */
    plain_with_salt = (unsigned char *) malloc(plain_size + 13);

    for (int i=0; i<12; i++) plain_with_salt[i] = (unsigned char) random() % 0x100;

    strcpy((char *)&plain_with_salt[12], plain);

    calc_sha_256(hash, plain_with_salt, plain_size +12);

    /* place the seed (base64 encoded) into first 16 bytes,
     * then '$' symbol, and then the base64 encoded hash
     * of the seed + pass (32 bytes unencoded -> 44 encoded.
     * Overall size is fixed, 16 + '$' + 44 + \0 == 62 */
    Base64encode(hash_string, (char *) plain_with_salt, 12);

    hash_string[16] = '$';

    Base64encode(&hash_string[17], (char*)hash, 32);

    free(plain_with_salt);
}

int check_passwd(char *plain, char *stored_string) {
    uint8_t hash[32];
    char hash_string[62];
    unsigned char *plain_with_salt;
    size_t plain_size;

    plain_size = strlen(plain);

    plain_with_salt = (unsigned char *) malloc(plain_size + 13);

    stored_string[16] = 0;
    Base64decode((char *)plain_with_salt, stored_string);
    stored_string[16] = '$';

    strcpy((char *) &plain_with_salt[12], plain);

    calc_sha_256(hash, plain_with_salt, plain_size +12);

    Base64encode(hash_string, (char *) plain_with_salt, 12);
    hash_string[16] = '$';
    Base64encode(&hash_string[17], (char *) hash, 32);

    free (plain_with_salt);

    return strcmp (hash_string, stored_string);
}
