/*
 * log.h
 *
 * Copyright 2017 Serge Ribalchenko <fisher@heim.in.ua>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef LOG_H
#define LOG_H

#include "config.h"

/* usual cases, info/warn/err reporting */
#define DBG_(...) if (cfg.verbose >2) { logit(3, __VA_ARGS__); }
#define INFO(...) if (cfg.verbose >1) { logit(2, __VA_ARGS__); }
#define WARN(...) if (cfg.verbose)    { logit(1, __VA_ARGS__); }
#define ERROR(...) do { logit(0, __VA_ARGS__); return (-1); } while (0)
#define ERR__(...) do { logit(0, __VA_ARGS__); } while (0)

/* this is for unexpected fatals or just unexpected WTFs with debug info */
#define FATAL(...) fatal(__FILE__, __LINE__, __func__, __VA_ARGS__)
#define WTF(...) wtf(__FILE__, __LINE__, __func__, __VA_ARGS__)

extern void logit (const char *, ...);
//extern void  logit (int, const char *, ...);
//extern void  fatal (const char *, int, const char*, const char *, ...);

#endif
