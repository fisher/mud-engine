/*
 * config.h
 *
 * Copyright 2019 Serge Rybalchenko <fisher@heim.in.ua>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#ifndef CONFIG_H
#define CONFIG_H

typedef struct engine_cfg ENGINE_CFG_T;
typedef ENGINE_CFG_T * engine_cfg_ptr;

struct engine_cfg {
    int verbose;        /* 0 errors only, 1 warnings, 2 info, 3 for debug */
    int daemonize;      /* 1 to go background, 0 would show the messages on tty */
    int logtype;        /* 0 means syslog, 1 logfile */
    char *logfile;      /* for logtype=1 this is the filename for logging */
    char *logdir;       /* directory for log files */
    char *userdir;      /* directory with user files */
    char *gods;         /* directory with known approved gods */
    char *world;        /* directory with area files */
    int port;           /* tcp port to listen for incoming connections */
    char *hostname;     /* short host name */
    char *fqdn;         /* fully qualified domain name */
    char *ip;           /* IP to bind to */
    char *rundir;       /* directory to work within after fork() */
    char *pidfile;      /* filename for pid number automation */
    char *exe_file;     /* argv[0] for copyover */
};

#ifndef CONFIG_C

/* global configuration */
extern volatile ENGINE_CFG_T cfg;

extern void cfg_init(int argc, char **argv);
extern int  cfg_read();

extern FILE *open_file( const char* base, const char* name,
                        const char* mode, const char* error_msg);

#endif

#endif

/*
 * Local Variables:
 * indent-tabs-mode: nil
 * show-trailing-whitespace: t
 * mode: c
 * End:
 *
 */
