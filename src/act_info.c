/***************************************************************************
 *  Original Diku Mud copyright (C) 1990, 1991 by Sebastian Hammer,        *
 *  Michael Seifert, Hans Henrik Strfeldt, Tom Madsen, and Katja Nyboe.    *
 *                                                                         *
 *  Merc Diku Mud improvments copyright (C) 1992, 1993 by Michael          *
 *  Chastain, Michael Quan, and Mitchell Tse.                              *
 *                                                                         *
 *  In order to use any part of this Merc Diku Mud, you must comply with   *
 *  both the original Diku license in 'license.doc' as well the Merc       *
 *  license in 'license.txt'.  In particular, you may not remove either of *
 *  these copyright notices.                                               *
 *                                                                         *
 *  Much time and thought has gone into this software and you are          *
 *  benefitting.  We hope that you share your changes too.  What goes      *
 *  around, comes around.                                                  *
 **************************************************************************/

/***************************************************************************
 *   ROM 2.4 is copyright 1993-1998 Russ Taylor                            *
 *   ROM has been brought to you by the ROM consortium                     *
 *       Russ Taylor (rtaylor@hypercube.org)                               *
 *       Gabrielle Taylor (gtaylor@hypercube.org)                          *
 *       Brian Moore (zump@rom.org)                                        *
 *   By using this code, you have agreed to follow the terms of the        *
 *   ROM license, in the file Rom24/doc/rom.license                        *
 **************************************************************************/

/*   QuickMUD - The Lazy Man's ROM - $Id: act_info.c,v 1.3 2000/12/01 10:48:33 ring0 Exp $ */

#ifdef __linux__
#define _XOPEN_SOURCE
#elif __NetBSD__
#pragma message "NetBSD case"
#endif

#include <unistd.h>
#include <sys/types.h>
#include <sys/time.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
#include "merc.h"
#include "interp.h"
#include "magic.h"
#include "recycle.h"
#include "tables.h"
#include "lookup.h"
#include "version.h"
#include "util.h"

char *const where_name[] = {
    "<used as light>     ",
    "<worn on finger>    ",
    "<worn on finger>    ",
    "<worn around neck>  ",
    "<worn around neck>  ",
    "<worn on torso>     ",
    "<worn on head>      ",
    "<worn on legs>      ",
    "<worn on feet>      ",
    "<worn on hands>     ",
    "<worn on arms>      ",
    "<worn as shield>    ",
    "<worn about body>   ",
    "<worn about waist>  ",
    "<worn around wrist> ",
    "<worn around wrist> ",
    "<wielded>           ",
    "<held>              ",
    "<floating nearby>   ",
};


/* for  keeping track of the player count */
int max_on = 0;

/*
 * Local functions.
 */
char *format_obj_to_char args ((OBJ_DATA * obj, CHAR_DATA * ch, bool fShort));
void show_list_to_char args ((OBJ_DATA * list, CHAR_DATA * ch,
                              bool fShort, bool fShowNothing));
void show_char_to_char_0 args ((CHAR_DATA * victim, CHAR_DATA * ch));
void show_char_to_char_1 args ((CHAR_DATA * victim, CHAR_DATA * ch));
void show_char_to_char args ((CHAR_DATA * list, CHAR_DATA * ch));
bool check_blind args ((CHAR_DATA * ch));



char *format_obj_to_char (OBJ_DATA * obj, CHAR_DATA * ch, bool fShort)
{
    static char buf[MAX_STRING_LENGTH];

    buf[0] = '\0';

    if ((fShort && (obj->short_descr == NULL || obj->short_descr[0] == '\0'))
        || (obj->description == NULL || obj->description[0] == '\0'))
        return buf;

    if (IS_OBJ_STAT (obj, ITEM_INVIS))
        strcat (buf, "{D(Invis) ");
    if (IS_AFFECTED (ch, AFF_DETECT_EVIL) && IS_OBJ_STAT (obj, ITEM_EVIL))
        strcat (buf, "{r(Red Aura) ");
    if (IS_AFFECTED (ch, AFF_DETECT_GOOD) && IS_OBJ_STAT (obj, ITEM_BLESS))
        strcat (buf, "{b(Blue Aura) ");
    if (IS_AFFECTED (ch, AFF_DETECT_MAGIC) && IS_OBJ_STAT (obj, ITEM_MAGIC))
        strcat (buf, "{C(Magical) ");
    if (IS_OBJ_STAT (obj, ITEM_GLOW))
        strcat (buf, "{R(Glowing) ");
    if (IS_OBJ_STAT (obj, ITEM_HUM))
        strcat (buf, "{B(Humming) ");

    strcat (buf, "{g");
    if (fShort)
    {
        if (obj->short_descr != NULL)
            strcat (buf, obj->short_descr);
    }
    else
    {
        if (obj->description != NULL)
            strcat (buf, obj->description);
    }
    strcat (buf, "{x");

    return buf;
}



/*
 * Show a list to a character.
 * Can coalesce duplicated items.
 */
void show_list_to_char (OBJ_DATA * list, CHAR_DATA * ch, bool fShort,
                        bool fShowNothing)
{
    char buf[MAX_STRING_LENGTH];
    BUFFER *output;
    char **prgpstrShow;
    int *prgnShow;
    char *pstrShow;
    OBJ_DATA *obj;
    int nShow;
    int iShow;
    int count;
    bool fCombine;

    if (ch->desc == NULL)
        return;

    /*
     * Alloc space for output lines.
     */
    output = new_buf ();

    count = 0;
    for (obj = list; obj != NULL; obj = obj->next_content)
        count++;
    prgpstrShow = alloc_mem (count * sizeof (char *));
    prgnShow = alloc_mem (count * sizeof (int));
    nShow = 0;

    /*
     * Format the list of objects.
     */
    for (obj = list; obj != NULL; obj = obj->next_content)
    {
        if (obj->wear_loc == WEAR_NONE && can_see_obj (ch, obj))
        {
            pstrShow = format_obj_to_char (obj, ch, fShort);

            fCombine = FALSE;

            if (IS_NPC (ch) || IS_SET (ch->comm, COMM_COMBINE))
            {
                /*
                 * Look for duplicates, case sensitive.
                 * Matches tend to be near end so run loop backwords.
                 */
                for (iShow = nShow - 1; iShow >= 0; iShow--)
                {
                    if (!strcmp (prgpstrShow[iShow], pstrShow))
                    {
                        prgnShow[iShow]++;
                        fCombine = TRUE;
                        break;
                    }
                }
            }

            /*
             * Couldn't combine, or didn't want to.
             */
            if (!fCombine)
            {
                prgpstrShow[nShow] = str_dup (pstrShow);
                prgnShow[nShow] = 1;
                nShow++;
            }
        }
    }

    /*
     * Output the formatted list.
     */
    for (iShow = 0; iShow < nShow; iShow++)
    {
        if (prgpstrShow[iShow][0] == '\0')
        {
            free_string (prgpstrShow[iShow]);
            continue;
        }

        if (IS_NPC (ch) || IS_SET (ch->comm, COMM_COMBINE))
        {
            if (prgnShow[iShow] != 1)
            {
                sprintf (buf, "(%2d) ", prgnShow[iShow]);
                add_buf (output, buf);
            }
            else
            {
                add_buf (output, "     ");
            }
        }
        add_buf (output, prgpstrShow[iShow]);
        add_buf (output, "\r\n");
        free_string (prgpstrShow[iShow]);
    }

    if (fShowNothing && nShow == 0)
    {
        if (IS_NPC (ch) || IS_SET (ch->comm, COMM_COMBINE))
            send_to_char ("     ", ch);
        send_to_char ("Nothing.\r\n", ch);
    }
    page_to_char (buf_string (output), ch);

    /*
     * Clean up.
     */
    free_buf (output);
    free_mem (prgpstrShow, count * sizeof (char *));
    free_mem (prgnShow, count * sizeof (int));

    return;
}



void show_char_to_char_0 (CHAR_DATA * victim, CHAR_DATA * ch)
{
    char buf[MAX_STRING_LENGTH], message[MAX_STRING_LENGTH];

    buf[0] = '\0';

    if (IS_SET (victim->comm, COMM_AFK))
        strcat (buf, "{b[AFK] ");
    if (IS_AFFECTED (victim, AFF_INVISIBLE))
        strcat (buf, "{D(Invis) ");
    if (victim->invis_level >= LEVEL_HERO)
        strcat (buf, "(Wizi) ");
    if (IS_AFFECTED (victim, AFF_HIDE))
        strcat (buf, "{D(Hide) ");
    if (IS_AFFECTED (victim, AFF_CHARM))
        strcat (buf, "(Charmed) ");
    if (IS_AFFECTED (victim, AFF_PASS_DOOR))
        strcat (buf, "{D(Translucent) ");
    if (IS_AFFECTED (victim, AFF_FAERIE_FIRE))
        strcat (buf, "{M(Pink Aura) ");
    if (IS_EVIL (victim) && IS_AFFECTED (ch, AFF_DETECT_EVIL))
        strcat (buf, "{r(Red Aura) ");
    if (IS_GOOD (victim) && IS_AFFECTED (ch, AFF_DETECT_GOOD))
        strcat (buf, "{Y(Golden Aura) ");
    if (IS_AFFECTED (victim, AFF_SANCTUARY))
        strcat (buf, "{W(White Aura) ");
    if (!IS_NPC (victim) && IS_SET (victim->act, PLR_KILLER))
        strcat (buf, "{R(KILLER) ");
    if (!IS_NPC (victim) && IS_SET (victim->act, PLR_THIEF))
        strcat (buf, "{B(THIEF) ");

    strcat (buf, "{y");
    if (victim->position == victim->start_pos
        && victim->long_descr[0] != '\0')
    {
        strcat (buf, victim->long_descr);
        strcat (buf, "{x");
        send_to_char (buf, ch);
        return;
    }

    strcat (buf, PERS (victim, ch));
    if (!IS_NPC (victim) && !IS_SET (ch->comm, COMM_BRIEF)
        && victim->position == POS_STANDING && ch->on == NULL)
        strcat (buf, victim->pcdata->title);

    switch (victim->position)
    {
        case POS_DEAD:
            strcat (buf, "{y is DEAD!!");
            break;
        case POS_MORTAL:
            strcat (buf, "{y is mortally wounded.");
            break;
        case POS_INCAP:
            strcat (buf, "{y is incapacitated.");
            break;
        case POS_STUNNED:
            strcat (buf, "{y is lying here stunned.");
            break;
        case POS_SLEEPING:
            if (victim->on != NULL)
            {
                if (IS_SET (victim->on->value[2], SLEEP_AT))
                {
                    sprintf (message, "{y is sleeping at %s.",
                             victim->on->short_descr);
                    strcat (buf, message);
                }
                else if (IS_SET (victim->on->value[2], SLEEP_ON))
                {
                    sprintf (message, "{y is sleeping on %s.",
                             victim->on->short_descr);
                    strcat (buf, message);
                }
                else
                {
                    sprintf (message, "{y is sleeping in %s.",
                             victim->on->short_descr);
                    strcat (buf, message);
                }
            }
            else
                strcat (buf, "{y is sleeping here.");
            break;
        case POS_RESTING:
            if (victim->on != NULL)
            {
                if (IS_SET (victim->on->value[2], REST_AT))
                {
                    sprintf (message, "{y is resting at %s.",
                             victim->on->short_descr);
                    strcat (buf, message);
                }
                else if (IS_SET (victim->on->value[2], REST_ON))
                {
                    sprintf (message, "{y is resting on %s.",
                             victim->on->short_descr);
                    strcat (buf, message);
                }
                else
                {
                    sprintf (message, "{y is resting in %s.",
                             victim->on->short_descr);
                    strcat (buf, message);
                }
            }
            else
                strcat (buf, "{y is resting here.");
            break;
        case POS_SITTING:
            if (victim->on != NULL)
            {
                if (IS_SET (victim->on->value[2], SIT_AT))
                {
                    sprintf (message, "{y is sitting at %s.",
                             victim->on->short_descr);
                    strcat (buf, message);
                }
                else if (IS_SET (victim->on->value[2], SIT_ON))
                {
                    sprintf (message, "{y is sitting on %s.",
                             victim->on->short_descr);
                    strcat (buf, message);
                }
                else
                {
                    sprintf (message, "{y is sitting in %s.",
                             victim->on->short_descr);
                    strcat (buf, message);
                }
            }
            else
                strcat (buf, "{y is sitting here.");
            break;
        case POS_STANDING:
            if (victim->on != NULL)
            {
                if (IS_SET (victim->on->value[2], STAND_AT))
                {
                    sprintf (message, "{y is standing at %s.",
                             victim->on->short_descr);
                    strcat (buf, message);
                }
                else if (IS_SET (victim->on->value[2], STAND_ON))
                {
                    sprintf (message, "{y is standing on %s.",
                             victim->on->short_descr);
                    strcat (buf, message);
                }
                else
                {
                    sprintf (message, "{y is standing in %s.",
                             victim->on->short_descr);
                    strcat (buf, message);
                }
            }
            else
                strcat (buf, "{y is here.");
            break;
        case POS_FIGHTING:
            strcat (buf, "{y is here, {rfighting{y ");
            if (victim->fighting == NULL)
                strcat (buf, "thin air??");
            else if (victim->fighting == ch)
                strcat (buf, "{RYOU{y!");
            else if (victim->in_room == victim->fighting->in_room)
            {
                strcat (buf, PERS (victim->fighting, ch));
                strcat (buf, ".");
            }
            else
                strcat (buf, "someone who left??");
            break;
    }

    strcat (buf, "{x\r\n");
    buf[0] = UPPER (buf[0]);
    send_to_char (buf, ch);
    return;
}



void show_char_to_char_1 (CHAR_DATA * victim, CHAR_DATA * ch)
{
    char buf[MAX_STRING_LENGTH];
    OBJ_DATA *obj;
    int iWear;
    int percent;
    bool found;

    if (can_see (victim, ch))
    {
        if (ch == victim)
            act ("$n looks at $mself.", ch, NULL, NULL, TO_ROOM);
        else
        {
            act ("$n looks at you.", ch, NULL, victim, TO_VICT);
            act ("$n looks at $N.", ch, NULL, victim, TO_NOTVICT);
        }
    }

    if (victim->description[0] != '\0')
    {
        send_to_char (victim->description, ch);
    }
    else
    {
        act ("You see nothing special about $M.", ch, NULL, victim, TO_CHAR);
    }

    if (victim->max_hit > 0)
        percent = (100 * victim->hit) / victim->max_hit;
    else
        percent = -1;

    strcpy (buf, PERS (victim, ch));

    if (percent >= 100)
        strcat (buf, " is in excellent condition.\r\n");
    else if (percent >= 90)
        strcat (buf, " has a few scratches.\r\n");
    else if (percent >= 75)
        strcat (buf, " has some small wounds and bruises.\r\n");
    else if (percent >= 50)
        strcat (buf, " has quite a few wounds.\r\n");
    else if (percent >= 30)
        strcat (buf, " has some big nasty wounds and scratches.\r\n");
    else if (percent >= 15)
        strcat (buf, " looks pretty hurt.\r\n");
    else if (percent >= 0)
        strcat (buf, " is in awful condition.\r\n");
    else
        strcat (buf, " is bleeding to death.\r\n");

    buf[0] = UPPER (buf[0]);
    send_to_char (buf, ch);

    found = FALSE;
    for (iWear = 0; iWear < MAX_WEAR; iWear++)
    {
        if ((obj = get_eq_char (victim, iWear)) != NULL
            && can_see_obj (ch, obj))
        {
            if (!found)
            {
                send_to_char ("\r\n", ch);
                act ("$N is using:", ch, NULL, victim, TO_CHAR);
                found = TRUE;
            }
            send_to_char (where_name[iWear], ch);
            send_to_char (format_obj_to_char (obj, ch, TRUE), ch);
            send_to_char ("\r\n", ch);
        }
    }

    if (victim != ch && !IS_NPC (ch)
        && number_percent () < get_skill (ch, gsn_peek))
    {
        send_to_char ("\r\nYou peek at the inventory:\r\n", ch);
        check_improve (ch, gsn_peek, TRUE, 4);
        show_list_to_char (victim->carrying, ch, TRUE, TRUE);
    }

    return;
}



void show_char_to_char (CHAR_DATA * list, CHAR_DATA * ch)
{
    CHAR_DATA *rch;

    for (rch = list; rch != NULL; rch = rch->next_in_room)
    {
        if (rch == ch)
            continue;

        if (get_trust (ch) < rch->invis_level)
            continue;

        if (can_see (ch, rch))
        {
            show_char_to_char_0 (rch, ch);
        }
        else if (room_is_dark (ch->in_room)
                 && IS_AFFECTED (rch, AFF_INFRARED))
        {
            send_to_char ("You see glowing {rred eyes{x watching YOU!\r\n", ch);
        }
    }

    return;
}



bool check_blind (CHAR_DATA * ch)
{

    if (!IS_NPC (ch) && IS_SET (ch->act, PLR_HOLYLIGHT))
        return TRUE;

    if (IS_AFFECTED (ch, AFF_BLIND))
    {
        send_to_char ("You can't see a thing!\r\n", ch);
        return FALSE;
    }

    return TRUE;
}

/* changes your scrolling preferences */
void do_scroll (CHAR_DATA * ch, char *argument)
{
    char arg[MAX_INPUT_LENGTH];
    char buf[100];
    int lines;

    one_argument (argument, arg);

    if (arg[0] == '\0')
    {
        if (ch->lines == 0)
            send_to_char ("You do not page long messages.\r\n", ch);
        else
        {
            sprintf (buf, "You currently display %d lines per page.\r\n",
                     ch->lines + 2);
            send_to_char (buf, ch);
        }
        return;
    }

    if (!is_number (arg))
    {
        send_to_char ("You must provide a number.\r\n", ch);
        return;
    }

    lines = atoi (arg);

    if (lines == 0)
    {
        send_to_char ("Paging disabled.\r\n", ch);
        ch->lines = 0;
        return;
    }

    if (lines < 10 || lines > 100)
    {
        send_to_char ("You must provide a reasonable number.\r\n", ch);
        return;
    }

    sprintf (buf, "Scroll set to %d lines.\r\n", lines);
    send_to_char (buf, ch);
    ch->lines = lines - 2;
}

/* RT does socials */
void do_socials (CHAR_DATA * ch, char *argument)
{
    char buf[MAX_STRING_LENGTH];
    int iSocial;
    int col;

    col = 0;

    for (iSocial = 0; social_table[iSocial].name[0] != '\0'; iSocial++)
    {
        sprintf (buf, "%-12.12s", social_table[iSocial].name);
        send_to_char (buf, ch);
        if (++col % 6 == 0)
            send_to_char ("\r\n", ch);
    }

    if (col % 6 != 0)
        send_to_char ("\r\n", ch);
    return;
}



/* RT Commands to replace news, motd, imotd, etc from ROM */

void do_motd (CHAR_DATA * ch, char *argument)
{
    do_function (ch, &do_help, "motd");
}

void do_imotd (CHAR_DATA * ch, char *argument)
{
    do_function (ch, &do_help, "imotd");
}

void do_rules (CHAR_DATA * ch, char *argument)
{
    do_function (ch, &do_help, "rules");
}

void do_story (CHAR_DATA * ch, char *argument)
{
    do_function (ch, &do_help, "story");
}

void do_wizlist (CHAR_DATA * ch, char *argument)
{
    do_function (ch, &do_help, "wizlist");
}

/* RT this following section holds all the auto commands from ROM, as well as
   replacements for config */

void do_autolist (CHAR_DATA * ch, char *argument)
{
    /* lists most player flags */
    if (IS_NPC (ch))
        return;

    send_to_char ("   action     status\r\n", ch);
    send_to_char ("---------------------\r\n", ch);

    send_to_char ("autoassist     ", ch);
    if (IS_SET (ch->act, PLR_AUTOASSIST))
        send_to_char ("{GON{x\r\n", ch);
    else
        send_to_char ("{ROFF{x\r\n", ch);

    send_to_char ("autoexit       ", ch);
    if (IS_SET (ch->act, PLR_AUTOEXIT))
        send_to_char ("{GON{x\r\n", ch);
    else
        send_to_char ("{ROFF{x\r\n", ch);

    send_to_char ("autogold       ", ch);
    if (IS_SET (ch->act, PLR_AUTOGOLD))
        send_to_char ("{GON{x\r\n", ch);
    else
        send_to_char ("{ROFF{x\r\n", ch);

    send_to_char ("autoloot       ", ch);
    if (IS_SET (ch->act, PLR_AUTOLOOT))
        send_to_char ("{GON{x\r\n", ch);
    else
        send_to_char ("{ROFF{x\r\n", ch);

    send_to_char ("autosac        ", ch);
    if (IS_SET (ch->act, PLR_AUTOSAC))
        send_to_char ("{GON{x\r\n", ch);
    else
        send_to_char ("{ROFF{x\r\n", ch);

    send_to_char ("autosplit      ", ch);
    if (IS_SET (ch->act, PLR_AUTOSPLIT))
        send_to_char ("{GON{x\r\n", ch);
    else
        send_to_char ("{ROFF{x\r\n", ch);

    send_to_char ("telnetga       ", ch);
    if (IS_SET (ch->comm, COMM_TELNET_GA))
        send_to_char ("{GON{x\r\n", ch);
    else
        send_to_char ("{ROFF{x\r\n",ch);

    send_to_char ("compact mode   ", ch);
    if (IS_SET (ch->comm, COMM_COMPACT))
        send_to_char ("{GON{x\r\n", ch);
    else
        send_to_char ("{ROFF{x\r\n", ch);

    send_to_char ("prompt         ", ch);
    if (IS_SET (ch->comm, COMM_PROMPT))
        send_to_char ("{GON{x\r\n", ch);
    else
        send_to_char ("{ROFF{x\r\n", ch);

    send_to_char ("combine items  ", ch);
    if (IS_SET (ch->comm, COMM_COMBINE))
        send_to_char ("{GON{x\r\n", ch);
    else
        send_to_char ("{ROFF{x\r\n", ch);

    if (!IS_SET (ch->act, PLR_CANLOOT))
        send_to_char ("Your corpse is safe from thieves.\r\n", ch);
    else
        send_to_char ("Your corpse may be looted.\r\n", ch);

    if (IS_SET (ch->act, PLR_NOSUMMON))
        send_to_char ("You cannot be summoned.\r\n", ch);
    else
        send_to_char ("You can be summoned.\r\n", ch);

    if (IS_SET (ch->act, PLR_NOFOLLOW))
        send_to_char ("You do not welcome followers.\r\n", ch);
    else
        send_to_char ("You accept followers.\r\n", ch);
}

void do_autoassist (CHAR_DATA * ch, char *argument)
{
    if (IS_NPC (ch))
        return;

    if (IS_SET (ch->act, PLR_AUTOASSIST))
    {
        send_to_char ("Autoassist removed.\r\n", ch);
        REMOVE_BIT (ch->act, PLR_AUTOASSIST);
    }
    else
    {
        send_to_char ("You will now assist when needed.\r\n", ch);
        SET_BIT (ch->act, PLR_AUTOASSIST);
    }
}

void do_autoexit (CHAR_DATA * ch, char *argument)
{
    if (IS_NPC (ch))
        return;

    if (IS_SET (ch->act, PLR_AUTOEXIT))
    {
        send_to_char ("Exits will no longer be displayed.\r\n", ch);
        REMOVE_BIT (ch->act, PLR_AUTOEXIT);
    }
    else
    {
        send_to_char ("Exits will now be displayed.\r\n", ch);
        SET_BIT (ch->act, PLR_AUTOEXIT);
    }
}

void do_autogold (CHAR_DATA * ch, char *argument)
{
    if (IS_NPC (ch))
        return;

    if (IS_SET (ch->act, PLR_AUTOGOLD))
    {
        send_to_char ("Autogold removed.\r\n", ch);
        REMOVE_BIT (ch->act, PLR_AUTOGOLD);
    }
    else
    {
        send_to_char ("Automatic gold looting set.\r\n", ch);
        SET_BIT (ch->act, PLR_AUTOGOLD);
    }
}

void do_autoloot (CHAR_DATA * ch, char *argument)
{
    if (IS_NPC (ch))
        return;

    if (IS_SET (ch->act, PLR_AUTOLOOT))
    {
        send_to_char ("Autolooting removed.\r\n", ch);
        REMOVE_BIT (ch->act, PLR_AUTOLOOT);
    }
    else
    {
        send_to_char ("Automatic corpse looting set.\r\n", ch);
        SET_BIT (ch->act, PLR_AUTOLOOT);
    }
}

void do_autosac (CHAR_DATA * ch, char *argument)
{
    if (IS_NPC (ch))
        return;

    if (IS_SET (ch->act, PLR_AUTOSAC))
    {
        send_to_char ("Autosacrificing removed.\r\n", ch);
        REMOVE_BIT (ch->act, PLR_AUTOSAC);
    }
    else
    {
        send_to_char ("Automatic corpse sacrificing set.\r\n", ch);
        SET_BIT (ch->act, PLR_AUTOSAC);
    }
}

void do_autosplit (CHAR_DATA * ch, char *argument)
{
    if (IS_NPC (ch))
        return;

    if (IS_SET (ch->act, PLR_AUTOSPLIT))
    {
        send_to_char ("Autosplitting removed.\r\n", ch);
        REMOVE_BIT (ch->act, PLR_AUTOSPLIT);
    }
    else
    {
        send_to_char ("Automatic gold splitting set.\r\n", ch);
        SET_BIT (ch->act, PLR_AUTOSPLIT);
    }
}

void do_autoall (CHAR_DATA *ch, char * argument)
{
    if (IS_NPC(ch))
        return;

    if (!strcmp (argument, "on"))
    {
        SET_BIT(ch->act,PLR_AUTOASSIST);
        SET_BIT(ch->act,PLR_AUTOEXIT);
        SET_BIT(ch->act,PLR_AUTOGOLD);
        SET_BIT(ch->act,PLR_AUTOLOOT);
        SET_BIT(ch->act,PLR_AUTOSAC);
        SET_BIT(ch->act,PLR_AUTOSPLIT);

        send_to_char("All autos turned on.\r\n",ch);
    }
    else if (!strcmp (argument, "off"))
    {
        REMOVE_BIT (ch->act, PLR_AUTOASSIST);
        REMOVE_BIT (ch->act, PLR_AUTOEXIT);
        REMOVE_BIT (ch->act, PLR_AUTOGOLD);
        REMOVE_BIT (ch->act, PLR_AUTOLOOT);
        REMOVE_BIT (ch->act, PLR_AUTOSAC);
        REMOVE_BIT (ch->act, PLR_AUTOSPLIT);

        send_to_char("All autos turned off.\r\n", ch);
    }
    else
        send_to_char("Usage: autoall [on|off]\r\n", ch);
}

void do_preferences (CHAR_DATA *ch, char *argument)
{
    char *help=
         "Usage:\r\n"
         "    {Cpreferences show{x     -- show the current settings\r\n"
         "    {Cpreferences score{x    -- set the score command look\r\n"
         "    {Cpreferences channels{x -- channel prefs\r\n"
         "";

    if (argument[0] == '\0') {
        send_to_char( help, ch );
    } else if (!strcmp (argument, "show")) {
        if (IS_SET (ch->comm, COMM_SHOW_AFFECTS)) {
            send_to_char("  score affects: {gON{x\r\n", ch);
        } else {
            send_to_char("  score affects: {rOFF{x\r\n", ch);
        }
        send_to_char( "other: {RNot yet implemented{x.\r\n", ch );
    } else if (!strcmp (argument, "score")) {
        if (IS_SET (ch->comm, COMM_SHOW_AFFECTS))
        {
            send_to_char ("Affects will no longer be shown in score.\r\n", ch);
            REMOVE_BIT (ch->comm, COMM_SHOW_AFFECTS);
        }
        else
        {
            send_to_char ("Affects will now be shown in score.\r\n", ch);
            SET_BIT (ch->comm, COMM_SHOW_AFFECTS);
        }
    } else {
        send_to_char( help, ch );
    }

    return;
}

void do_brief (CHAR_DATA * ch, char *argument)
{
    if (IS_SET (ch->comm, COMM_BRIEF))
    {
        send_to_char ("Full descriptions activated.\r\n", ch);
        REMOVE_BIT (ch->comm, COMM_BRIEF);
    }
    else
    {
        send_to_char ("Short descriptions activated.\r\n", ch);
        SET_BIT (ch->comm, COMM_BRIEF);
    }
}

void do_compact (CHAR_DATA * ch, char *argument)
{
    if (IS_SET (ch->comm, COMM_COMPACT))
    {
        send_to_char ("Compact mode removed.\r\n", ch);
        REMOVE_BIT (ch->comm, COMM_COMPACT);
    }
    else
    {
        send_to_char ("Compact mode set.\r\n", ch);
        SET_BIT (ch->comm, COMM_COMPACT);
    }
}

void do_show (CHAR_DATA * ch, char *argument)
{
    char buf[MAX_STRING_LENGTH];
    char *help=
         "Usage:\r\n"
         "    {Cshow build{x     -- show build info\r\n"
         "    {Cshow server{x    -- underlying server info\r\n"
         "    {Cshow activity{x  -- activity stats\r\n"
         "";

    if (argument[0] == '\0') {
        send_to_char( help, ch );
    } else if (!strcmp (argument, "build")) {
        /* REVISION macro is set each make run by -D option to gcc */
        sprintf( buf,
            "Engine info:\r\n"
            "    Version: %s \"%s\", src rev: " REVISION "\r\n"
            "    Build timestamp: %s\r\n"
            "    Build hostname: %s\r\n"
            "    Build OS info: %s %s\r\n",
            version(), release(), build_time(),
            build_hostname(), build_kernel(), build_kernel_release());
        send_to_char( buf, ch );
    } else if (!strcmp (argument, "server")) {
        send_to_char(
            "Server info:\r\n"
            "", ch);
    } else {
        send_to_char( help, ch );
    }

    return;
}

void do_prompt (CHAR_DATA * ch, char *argument)
{
    char buf[MAX_STRING_LENGTH];

    if (argument[0] == '\0')
    {
        if (IS_SET (ch->comm, COMM_PROMPT))
        {
            send_to_char ("You will no longer see prompts.\r\n", ch);
            REMOVE_BIT (ch->comm, COMM_PROMPT);
        }
        else
        {
            send_to_char ("You will now see prompts.\r\n", ch);
            SET_BIT (ch->comm, COMM_PROMPT);
        }
        return;
    }

    if (!strcmp (argument, "all"))
        strcpy (buf, "%h/%H %m/%M %v/%V {Y%g{c.{D%s {c+%X {x%e%c");
    else if (!strcmp (argument, "full"))
        strcpy (buf, "%F{x %h/%H %m/%M %v/%V {Y%g{c.{D%s {c+%X {x[%D, %t:%T] %e%c");
    else if (!strcmp (argument, "std"))
        strcpy (buf, "<%hhp %mm %vmv> ");
    else
    {
        if (strlen (argument) > 100)
            argument[100] = '\0';
        strcpy (buf, argument);
        smash_tilde (buf);
        if (str_suffix ("%c", buf))
            strcat (buf, " ");

    }

    free_string (ch->prompt);
    ch->prompt = str_dup (buf);
    sprintf (buf, "Prompt set to %s\r\n", ch->prompt);
    send_to_char (buf, ch);
    return;
}

void do_combine (CHAR_DATA * ch, char *argument)
{
    if (IS_SET (ch->comm, COMM_COMBINE))
    {
        send_to_char ("Long inventory selected.\r\n", ch);
        REMOVE_BIT (ch->comm, COMM_COMBINE);
    }
    else
    {
        send_to_char ("Combined inventory selected.\r\n", ch);
        SET_BIT (ch->comm, COMM_COMBINE);
    }
}

void do_noloot (CHAR_DATA * ch, char *argument)
{
    if (IS_NPC (ch))
        return;

    if (IS_SET (ch->act, PLR_CANLOOT))
    {
        send_to_char ("Your corpse is now safe from thieves.\r\n", ch);
        REMOVE_BIT (ch->act, PLR_CANLOOT);
    }
    else
    {
        send_to_char ("Your corpse may now be looted.\r\n", ch);
        SET_BIT (ch->act, PLR_CANLOOT);
    }
}

void do_nofollow (CHAR_DATA * ch, char *argument)
{
    if (IS_NPC (ch))
        return;

    if (IS_SET (ch->act, PLR_NOFOLLOW))
    {
        send_to_char ("You now accept followers.\r\n", ch);
        REMOVE_BIT (ch->act, PLR_NOFOLLOW);
    }
    else
    {
        send_to_char ("You no longer accept followers.\r\n", ch);
        SET_BIT (ch->act, PLR_NOFOLLOW);
        die_follower (ch);
    }
}

void do_nosummon (CHAR_DATA * ch, char *argument)
{
    if (IS_NPC (ch))
    {
        if (IS_SET (ch->imm_flags, IMM_SUMMON))
        {
            send_to_char ("You are no longer immune to summon.\r\n", ch);
            REMOVE_BIT (ch->imm_flags, IMM_SUMMON);
        }
        else
        {
            send_to_char ("You are now immune to summoning.\r\n", ch);
            SET_BIT (ch->imm_flags, IMM_SUMMON);
        }
    }
    else
    {
        if (IS_SET (ch->act, PLR_NOSUMMON))
        {
            send_to_char ("You are no longer immune to summon.\r\n", ch);
            REMOVE_BIT (ch->act, PLR_NOSUMMON);
        }
        else
        {
            send_to_char ("You are now immune to summoning.\r\n", ch);
            SET_BIT (ch->act, PLR_NOSUMMON);
        }
    }
}

void do_look (CHAR_DATA * ch, char *argument)
{
    char buf[MAX_STRING_LENGTH];
    char arg1[MAX_INPUT_LENGTH];
    char arg2[MAX_INPUT_LENGTH];
    char arg3[MAX_INPUT_LENGTH];
    EXIT_DATA *pexit;
    CHAR_DATA *victim;
    OBJ_DATA *obj;
    char *pdesc;
    int door;
    int number, count;

    if (ch->desc == NULL)
        return;

    if (ch->position < POS_SLEEPING)
    {
        send_to_char ("You can't see anything but stars!\r\n", ch);
        return;
    }

    if (ch->position == POS_SLEEPING)
    {
        send_to_char ("You can't see anything, you're sleeping!\r\n", ch);
        return;
    }

    if (!check_blind (ch))
        return;

    if (!IS_NPC (ch)
        && !IS_SET (ch->act, PLR_HOLYLIGHT) && room_is_dark (ch->in_room))
    {
        send_to_char ("It is pitch black ... \r\n", ch);
        show_char_to_char (ch->in_room->people, ch);
        return;
    }

    argument = one_argument (argument, arg1);
    argument = one_argument (argument, arg2);
    number = number_argument (arg1, arg3);
    count = 0;

    if (arg1[0] == '\0' || !str_cmp (arg1, "auto"))
    {
        /* 'look' or 'look auto' */
        send_to_char ("{s", ch);
        send_to_char (ch->in_room->name, ch);
        send_to_char ("{x", ch);

        if ((IS_IMMORTAL (ch)
             && (IS_NPC (ch) || IS_SET (ch->act, PLR_HOLYLIGHT)))
            || IS_BUILDER (ch, ch->in_room->area))
        {
            sprintf (buf, "{r [{RRoom %d{r]{x", ch->in_room->vnum);
            send_to_char (buf, ch);
        }

        send_to_char ("\r\n", ch);

        if (arg1[0] == '\0'
            || (!IS_NPC (ch) && !IS_SET (ch->comm, COMM_BRIEF)))
        {
            send_to_char ("  ", ch);
            send_to_char ("{S", ch);
            send_to_char (ch->in_room->description, ch);
            send_to_char ("{x", ch);
        }

        if (!IS_NPC (ch) && IS_SET (ch->act, PLR_AUTOEXIT))
        {
            send_to_char ("\r\n", ch);
            do_function (ch, &do_exits, "auto");
        }

        show_list_to_char (ch->in_room->contents, ch, FALSE, FALSE);
        show_char_to_char (ch->in_room->people, ch);
        return;
    }

    if (!str_cmp (arg1, "i") || !str_cmp (arg1, "in")
        || !str_cmp (arg1, "on"))
    {
        /* 'look in' */
        if (arg2[0] == '\0')
        {
            send_to_char ("Look in what?\r\n", ch);
            return;
        }

        if ((obj = get_obj_here (ch, arg2)) == NULL)
        {
            send_to_char ("You do not see that here.\r\n", ch);
            return;
        }

        switch (obj->item_type)
        {
            default:
                send_to_char ("That is not a container.\r\n", ch);
                break;

            case ITEM_DRINK_CON:
                if (obj->value[1] <= 0)
                {
                    send_to_char ("It is empty.\r\n", ch);
                    break;
                }

                sprintf (buf, "It's %sfilled with  a %s liquid.\r\n",
                         obj->value[1] < obj->value[0] / 4
                         ? "less than half-" :
                         obj->value[1] < 3 * obj->value[0] / 4
                         ? "about half-" : "more than half-",
                         liq_table[obj->value[2]].liq_color);

                send_to_char (buf, ch);
                break;

            case ITEM_CONTAINER:
            case ITEM_CORPSE_NPC:
            case ITEM_CORPSE_PC:
                if (IS_SET (obj->value[1], CONT_CLOSED))
                {
                    send_to_char ("It is closed.\r\n", ch);
                    break;
                }

                act ("$p holds:", ch, obj, NULL, TO_CHAR);
                show_list_to_char (obj->contains, ch, TRUE, TRUE);
                break;
        }
        return;
    }

    if ((victim = get_char_room (ch, arg1)) != NULL)
    {
        show_char_to_char_1 (victim, ch);
        return;
    }

    for (obj = ch->carrying; obj != NULL; obj = obj->next_content)
    {
        if (can_see_obj (ch, obj))
        {                        /* player can see object */
            pdesc = get_extra_descr (arg3, obj->extra_descr);
            if (pdesc != NULL)
            {
                if (++count == number)
                {
                    send_to_char (pdesc, ch);
                    return;
                }
                else
                    continue;
            }

            pdesc = get_extra_descr (arg3, obj->pIndexData->extra_descr);
            if (pdesc != NULL)
            {
                if (++count == number)
                {
                    send_to_char (pdesc, ch);
                    return;
                }
                else
                    continue;
            }

            if (is_name (arg3, obj->name))
                if (++count == number)
                {
                    send_to_char (obj->description, ch);
                    send_to_char ("\r\n", ch);
                    return;
                }
        }
    }

    for (obj = ch->in_room->contents; obj != NULL; obj = obj->next_content)
    {
        if (can_see_obj (ch, obj))
        {
            pdesc = get_extra_descr (arg3, obj->extra_descr);
            if (pdesc != NULL)
                if (++count == number)
                {
                    send_to_char (pdesc, ch);
                    return;
                }

            pdesc = get_extra_descr (arg3, obj->pIndexData->extra_descr);
            if (pdesc != NULL)
                if (++count == number)
                {
                    send_to_char (pdesc, ch);
                    return;
                }

            if (is_name (arg3, obj->name))
                if (++count == number)
                {
                    send_to_char (obj->description, ch);
                    send_to_char ("\r\n", ch);
                    return;
                }
        }
    }

    pdesc = get_extra_descr (arg3, ch->in_room->extra_descr);
    if (pdesc != NULL)
    {
        if (++count == number)
        {
            send_to_char (pdesc, ch);
            return;
        }
    }

    if (count > 0 && count != number)
    {
        if (count == 1)
            sprintf (buf, "You only see one %s here.\r\n", arg3);
        else
            sprintf (buf, "You only see %d of those here.\r\n", count);

        send_to_char (buf, ch);
        return;
    }

    if (!str_cmp (arg1, "n") || !str_cmp (arg1, "north"))
        door = 0;
    else if (!str_cmp (arg1, "e") || !str_cmp (arg1, "east"))
        door = 1;
    else if (!str_cmp (arg1, "s") || !str_cmp (arg1, "south"))
        door = 2;
    else if (!str_cmp (arg1, "w") || !str_cmp (arg1, "west"))
        door = 3;
    else if (!str_cmp (arg1, "u") || !str_cmp (arg1, "up"))
        door = 4;
    else if (!str_cmp (arg1, "d") || !str_cmp (arg1, "down"))
        door = 5;
    else
    {
        send_to_char ("You do not see that here.\r\n", ch);
        return;
    }

    /* 'look direction' */
    if ((pexit = ch->in_room->exit[door]) == NULL)
    {
        send_to_char ("Nothing special there.\r\n", ch);
        return;
    }

    if (pexit->description != NULL && pexit->description[0] != '\0')
        send_to_char (pexit->description, ch);
    else
        send_to_char ("Nothing special there.\r\n", ch);

    if (pexit->keyword != NULL
        && pexit->keyword[0] != '\0' && pexit->keyword[0] != ' ')
    {
        if (IS_SET (pexit->exit_info, EX_CLOSED))
        {
            act ("The $d is closed.", ch, NULL, pexit->keyword, TO_CHAR);
        }
        else if (IS_SET (pexit->exit_info, EX_ISDOOR))
        {
            act ("The $d is open.", ch, NULL, pexit->keyword, TO_CHAR);
        }
    }

    return;
}

/* RT added back for the hell of it */
void do_read (CHAR_DATA * ch, char *argument)
{
    do_function (ch, &do_look, argument);
}

void do_examine (CHAR_DATA * ch, char *argument)
{
    char buf[MAX_STRING_LENGTH];
    char arg[MAX_INPUT_LENGTH];
    OBJ_DATA *obj;

    one_argument (argument, arg);

    if (arg[0] == '\0')
    {
        send_to_char ("Examine what?\r\n", ch);
        return;
    }

    do_function (ch, &do_look, arg);

    if ((obj = get_obj_here (ch, arg)) != NULL)
    {
        switch (obj->item_type)
        {
            default:
                break;

            case ITEM_JUKEBOX:
                do_function (ch, &do_play, "list");
                break;

            case ITEM_MONEY:
                if (obj->value[0] == 0)
                {
                    if (obj->value[1] == 0)
                        sprintf (buf,
                                 "Odd...there's no coins in the pile.\r\n");
                    else if (obj->value[1] == 1)
                        sprintf (buf, "Wow. One gold coin.\r\n");
                    else
                        sprintf (buf,
                                 "There are %d gold coins in the pile.\r\n",
                                 obj->value[1]);
                }
                else if (obj->value[1] == 0)
                {
                    if (obj->value[0] == 1)
                        sprintf (buf, "Wow. One silver coin.\r\n");
                    else
                        sprintf (buf,
                                 "There are %d silver coins in the pile.\r\n",
                                 obj->value[0]);
                }
                else
                    sprintf (buf,
                             "There are %d gold and %d silver coins in the pile.\r\n",
                             obj->value[1], obj->value[0]);
                send_to_char (buf, ch);
                break;

            case ITEM_DRINK_CON:
            case ITEM_CONTAINER:
            case ITEM_CORPSE_NPC:
            case ITEM_CORPSE_PC:
                sprintf (buf, "in %s", argument);
                do_function (ch, &do_look, buf);
        }
    }

    return;
}



/*
 * Thanks to Zrin for auto-exit part.
 */
void do_exits (CHAR_DATA * ch, char *argument)
{
    extern char *const dir_name[];
    char buf[MAX_STRING_LENGTH];
    EXIT_DATA *pexit;
    bool found;
    bool fAuto;
    int door;

    fAuto = !str_cmp (argument, "auto");

    if (!check_blind (ch))
        return;

    if (fAuto)
        sprintf (buf, "{o[Exits:");
    else if (IS_IMMORTAL (ch))
        sprintf (buf, "Obvious exits from room %d:\r\n", ch->in_room->vnum);
    else
        sprintf (buf, "Obvious exits:\r\n");

    found = FALSE;
    for (door = 0; door <= 5; door++)
    {
        if ((pexit = ch->in_room->exit[door]) != NULL
            && pexit->u1.to_room != NULL
            && can_see_room (ch, pexit->u1.to_room)
            && !IS_SET (pexit->exit_info, EX_CLOSED))
        {
            found = TRUE;
            if (fAuto)
            {
                strcat (buf, " ");
                strcat (buf, dir_name[door]);
            }
            else
            {
                sprintf (buf + strlen (buf), "%-5s - %s",
                         capitalize (dir_name[door]),
                         room_is_dark (pexit->u1.to_room)
                         ? "Too dark to tell" : pexit->u1.to_room->name);
                if (IS_IMMORTAL (ch))
                    sprintf (buf + strlen (buf),
                             " (room %d)\r\n", pexit->u1.to_room->vnum);
                else
                    sprintf (buf + strlen (buf), "\r\n");
            }
        }
    }

    if (!found)
        strcat (buf, fAuto ? " none" : "None.\r\n");

    if (fAuto)
        strcat (buf, "]{x\r\n");

    send_to_char (buf, ch);
    return;
}

void do_worth (CHAR_DATA * ch, char *argument)
{
    char buf[MAX_STRING_LENGTH];

    if (IS_NPC (ch))
    {
        sprintf (buf, "You have %ld gold and %ld silver.\r\n",
                 ch->gold, ch->silver);
        send_to_char (buf, ch);
        return;
    }

    sprintf (buf,
             "You have %ld gold, %ld silver, and %d experience (%d exp to level).\r\n",
             ch->gold, ch->silver, ch->exp,
             (ch->level + 1) * exp_per_level (ch,
                                              ch->pcdata->points) - ch->exp);

    send_to_char (buf, ch);

    return;
}

int strlen_colorless(char * str) {
    int len = 0, i = 0;

    while(str[i]) {
        if (str[i] == '{') {
            if (str[i+1] == '{') len++;
            i+=2;
        } else { i++; len++; }
    }

    return len;
}

void augment_string(char * str, CHAR_DATA * ch) {
    int i = 0;
    while(str[i]) {
        if (str[i] == '{') {
            if (str[i+1] == '{') {
                send_to_char(" ", ch);
            } else {
                send_to_char("  ", ch);
            }
            i+=2;
        } else {
            i++;
        }
    }
}

char *alignment_hr(int align) {
    if (align > 900)
        return "{Wangelic{x";
    else if (align > 700)
        return "{Ysaintly{x";
    else if (align > 350)
        return "{ygood{x";
    else if (align > 100)
        return "{yk{xind";
    else if (align > -100)
        return "{Dneutral{x";
    else if (align > -350)
        return "{rm{xean";
    else if (align > -700)
        return "{mevil{x";
    else if (align > -900)
        return "{rdemonic{x";
    else
        return "{Rsatanic{x";
}

void score_tabular (CHAR_DATA *ch)
{
    char buf[MAX_STRING_LENGTH];
    char *title;
    char *lb = "{c+-------------------------------------------------------------------------+\r\n";

    title = IS_NPC(ch) ? "" : ch->pcdata->title;
    send_to_char (lb, ch);
    sprintf (buf, "{c|{y%12s    {W%-45s", ch->name, title);
    send_to_char(buf, ch);
    augment_string(title, ch);
    sprintf (buf, "{G%5d y.o.  {c|\r\n", get_age(ch));
    send_to_char(buf , ch);

    if (IS_NPC(ch)) {
        sprintf (buf, "| level:{C%3d{c       experience:{C%8d{c to next level:{C%6d   {G%6d hours {c|\r\n",
                 ch->level, 0, 0, 0);
    } else {
        sprintf (buf, "| level:{C%3d{c       experience:{C%8d{c to next level:{C%6d   {G%6d hours {c|\r\n",
                 ch->level, ch->exp, (ch->level + 1) * exp_per_level (ch, ch->pcdata->points) - ch->exp,
                 (ch->played + (int) (current_time - ch->logon)) / 3600);
    }
    send_to_char(buf, ch);

    send_to_char (lb, ch);

    sprintf (buf, "| Race/sex :{Y%7s/%6s {c|{G str: {Y%2d{G(%2d) {c| Practice:{Y%4d {c| Bank  :{Y%7d {c|\r\n",
             race_table[ch->race].name,
             ch->sex == 0 ? "sexless" : ch->sex == 1 ? "male" : "female",
             ch->perm_stat[STAT_STR],
             get_curr_stat (ch, STAT_STR),
             ch->practice, 0);
    send_to_char (buf, ch);

    sprintf (buf, "| Class    :{Y%7s        {c|{G int: {Y%2d{G(%2d) {c| Training:{Y%4d {c| {YGold  :%7ld {c|\r\n",
             IS_NPC (ch) ? "mobile" : class_table[ch->class].name,
             ch->perm_stat[STAT_INT],
             get_curr_stat (ch, STAT_INT),
             ch->train, ch->gold);
    send_to_char (buf, ch);

    sprintf (buf, "| Align    :{Y%11s (%+5d){c|{G wis: {Y%2d{G(%2d) {c| Hitroll :{Y%4d {c| {DSilver:{Y%7ld {c|\r\n",
             alignment_hr(ch->alignment),
             ch->alignment,
             ch->perm_stat[STAT_WIS],
             get_curr_stat (ch, STAT_WIS),
             GET_HITROLL (ch), ch->silver);
    send_to_char (buf, ch);

    sprintf (buf, "| Deity    :{Y%12s   {c|{G dex: {Y%2d{G(%2d) {c| Damroll :{Y%4d {c|                |\r\n",
             "(none)",
             ch->perm_stat[STAT_DEX],
             get_curr_stat (ch, STAT_DEX),
             GET_DAMROLL (ch));
    send_to_char (buf, ch);

    sprintf (buf, "| Home     :{Y%14s {c|{G con: {Y%2d{G(%2d) {c| Saves   :{Y%4d {c|                |\r\n",
             "Midgaard",
             ch->perm_stat[STAT_CON],
             get_curr_stat (ch, STAT_CON),
             ch->saving_throw);
    send_to_char (buf, ch);

    send_to_char (lb, ch);

    sprintf (buf, "| HP    :{Y%4d/%4d {c| Pierce:{Y%5d {c| %-10s %-6s %-8s %-7s    |\r\n",
             ch->hit, ch->max_hit, GET_AC (ch, AC_PIERCE),
             position_table[ch->position].name,
             !IS_NPC (ch) && ch->pcdata->condition[COND_DRUNK] > 10 ? "drunk":"",
             !IS_NPC (ch) && ch->pcdata->condition[COND_THIRST] == 0 ? "thirsty":"",
             !IS_NPC (ch) && ch->pcdata->condition[COND_HUNGER] == 0 ? "hungry":"");
    send_to_char (buf, ch);

    sprintf (buf, "| Mana  :{Y%4d/%4d {c| Bash  :{Y%5d {c| %-37s |\r\n",
             ch->mana, ch->max_mana, GET_AC (ch, AC_BASH), "");
    send_to_char (buf, ch);

    sprintf (buf, "| Moves :{Y%4d/%4d {c| Slash :{Y%5d {c| %-37s |\r\n",
             ch->move, ch->max_move, GET_AC (ch, AC_SLASH), "");
    send_to_char (buf, ch);

    sprintf (buf, "| Inv   :{Y%4d/%4d {c| Magic :{Y%5d {c| %-37s |\r\n",
             ch->carry_number, can_carry_n (ch), GET_AC (ch, AC_EXOTIC), "");
    send_to_char (buf, ch);

    if (can_carry_w(ch) > 10000) {
        sprintf (buf, "| Weight:{Y%3ldk/%3dk {c| {MWimpy :{Y%5d {c| %-37s |\r\n",
                 get_carry_weight (ch) / 10000,
                 can_carry_w (ch) / 10000, ch->wimpy, "");
    } else {
        sprintf (buf, "| Weight:{Y%4ld/%4d {c| {MWimpy :{Y%5d {c| %-37s |\r\n",
                 get_carry_weight (ch) / 10,
                 can_carry_w (ch) / 10, ch->wimpy, "");
    }

    send_to_char (buf, ch);

    send_to_char (lb, ch);
    send_to_char ("{x\r\n", ch);
}

void score_plain(CHAR_DATA * ch)
{
    char buf[MAX_STRING_LENGTH];
    int i;

    sprintf (buf,
             "You are {y%s{W%s{x, level {W%d{x, {W%d{x years old ({W%d{x hours).\r\n",
             ch->name,
             IS_NPC (ch) ? "" : ch->pcdata->title,
             ch->level, get_age (ch),
             (ch->played + (int) (current_time - ch->logon)) / 3600);
    send_to_char (buf, ch);

    if (get_trust (ch) != ch->level)
    {
        sprintf (buf, "You are trusted at level {W%d{x.\r\n", get_trust (ch));
        send_to_char (buf, ch);
    }

    sprintf (buf, "Race: {W%s{x  Sex: {W%s{x  Class: {W%s{x\r\n",
             race_table[ch->race].name,
             ch->sex == 0 ? "sexless" : ch->sex == 1 ? "male" : "female",
             IS_NPC (ch) ? "mobile" : class_table[ch->class].name);
    send_to_char (buf, ch);


    sprintf (buf,
             "You have {W%d{c/%d{x hit, {W%d{c/%d{x mana, {W%d{c/%d{x movement.\r\n",
             ch->hit, ch->max_hit,
             ch->mana, ch->max_mana, ch->move, ch->max_move);
    send_to_char (buf, ch);

    sprintf (buf,
             "You have {W%d{x practices and {W%d{x training sessions.\r\n",
             ch->practice, ch->train);
    send_to_char (buf, ch);

    sprintf (buf,
             "You are carrying {W%d{c/%d{x items with weight {W%ld{c/%d{x standard imperial stones.\r\n",
             ch->carry_number, can_carry_n (ch),
             get_carry_weight (ch) / 10, can_carry_w (ch) / 10);
    send_to_char (buf, ch);

    sprintf (buf,
             "Str: {W%d{x({c%d{x)  Int: {W%d{x({c%d{x)  Wis: {W%d{x({c%d{x)  Dex: {W%d{x({c%d{x)  Con: {W%d{x({c%d{x)\r\n",
             ch->perm_stat[STAT_STR],
             get_curr_stat (ch, STAT_STR),
             ch->perm_stat[STAT_INT],
             get_curr_stat (ch, STAT_INT),
             ch->perm_stat[STAT_WIS],
             get_curr_stat (ch, STAT_WIS),
             ch->perm_stat[STAT_DEX],
             get_curr_stat (ch, STAT_DEX),
             ch->perm_stat[STAT_CON], get_curr_stat (ch, STAT_CON));
    send_to_char (buf, ch);

    sprintf (buf,
             "You have scored {W%d{x exp, and have {Y%ld{x gold and {D%ld{x silver coins.\r\n",
             ch->exp, ch->gold, ch->silver);
    send_to_char (buf, ch);

    /* RT shows exp to level */
    if (!IS_NPC (ch) && ch->level < LEVEL_HERO)
    {
        sprintf (buf,
                 "You need {c%d{x exp to level.\r\n",
                 ((ch->level + 1) * exp_per_level (ch, ch->pcdata->points) -
                  ch->exp));
        send_to_char (buf, ch);
    }

    sprintf (buf, "Wimpy set to {r%d{x hit points.\r\n", ch->wimpy);
    send_to_char (buf, ch);

    if (!IS_NPC (ch) && ch->pcdata->condition[COND_DRUNK] > 10)
        send_to_char ("You are drunk.\r\n", ch);
    if (!IS_NPC (ch) && ch->pcdata->condition[COND_THIRST] == 0)
        send_to_char ("You are thirsty.\r\n", ch);
    if (!IS_NPC (ch) && ch->pcdata->condition[COND_HUNGER] == 0)
        send_to_char ("You are hungry.\r\n", ch);

    switch (ch->position)
    {
        case POS_DEAD:
            send_to_char ("You are {RDEAD{x!!\r\n", ch);
            break;
        case POS_MORTAL:
            send_to_char ("You are mortally wounded.\r\n", ch);
            break;
        case POS_INCAP:
            send_to_char ("You are incapacitated.\r\n", ch);
            break;
        case POS_STUNNED:
            send_to_char ("You are stunned.\r\n", ch);
            break;
        case POS_SLEEPING:
            send_to_char ("You are sleeping.\r\n", ch);
            break;
        case POS_RESTING:
            send_to_char ("You are resting.\r\n", ch);
            break;
        case POS_SITTING:
            send_to_char ("You are sitting.\r\n", ch);
            break;
        case POS_STANDING:
            send_to_char ("You are standing.\r\n", ch);
            break;
        case POS_FIGHTING:
            send_to_char ("You are fighting.\r\n", ch);
            break;
    }


    /* print AC values */
    if (ch->level >= 25)
    {
        sprintf (buf, "Armor: pierce: {W%d{x  bash: {W%d{x  slash: {W%d{x  magic: {W%d{x\r\n",
                 GET_AC (ch, AC_PIERCE),
                 GET_AC (ch, AC_BASH),
                 GET_AC (ch, AC_SLASH), GET_AC (ch, AC_EXOTIC));
        send_to_char (buf, ch);
    }

    for (i = 0; i < 4; i++)
    {
        char *temp;

        switch (i)
        {
            case (AC_PIERCE):
                temp = "piercing";
                break;
            case (AC_BASH):
                temp = "bashing";
                break;
            case (AC_SLASH):
                temp = "slashing";
                break;
            case (AC_EXOTIC):
                temp = "magic";
                break;
            default:
                temp = "error";
                break;
        }

        send_to_char ("You are ", ch);

        if (GET_AC (ch, i) >= 101)
            sprintf (buf, "hopelessly vulnerable to %s.\r\n", temp);
        else if (GET_AC (ch, i) >= 80)
            sprintf (buf, "defenseless against %s.\r\n", temp);
        else if (GET_AC (ch, i) >= 60)
            sprintf (buf, "barely protected from %s.\r\n", temp);
        else if (GET_AC (ch, i) >= 40)
            sprintf (buf, "slightly armored against %s.\r\n", temp);
        else if (GET_AC (ch, i) >= 20)
            sprintf (buf, "somewhat armored against %s.\r\n", temp);
        else if (GET_AC (ch, i) >= 0)
            sprintf (buf, "armored against %s.\r\n", temp);
        else if (GET_AC (ch, i) >= -20)
            sprintf (buf, "well-armored against %s.\r\n", temp);
        else if (GET_AC (ch, i) >= -40)
            sprintf (buf, "very well-armored against %s.\r\n", temp);
        else if (GET_AC (ch, i) >= -60)
            sprintf (buf, "heavily armored against %s.\r\n", temp);
        else if (GET_AC (ch, i) >= -80)
            sprintf (buf, "superbly armored against %s.\r\n", temp);
        else if (GET_AC (ch, i) >= -100)
            sprintf (buf, "almost invulnerable to %s.\r\n", temp);
        else
            sprintf (buf, "divinely armored against %s.\r\n", temp);

        send_to_char (buf, ch);
    }


    /* RT wizinvis and holy light */
    if (IS_IMMORTAL (ch))
    {
        send_to_char ("Holy Light: ", ch);
        if (IS_SET (ch->act, PLR_HOLYLIGHT))
            send_to_char ("on", ch);
        else
            send_to_char ("off", ch);

        if (ch->invis_level)
        {
            sprintf (buf, "  Invisible: level %d", ch->invis_level);
            send_to_char (buf, ch);
        }

        if (ch->incog_level)
        {
            sprintf (buf, "  Incognito: level %d", ch->incog_level);
            send_to_char (buf, ch);
        }
        send_to_char ("\r\n", ch);
    }

    if (ch->level >= 15)
    {
        sprintf (buf, "Hitroll: {W%d{x  Damroll: {W%d{x.\r\n",
                 GET_HITROLL (ch), GET_DAMROLL (ch));
        send_to_char (buf, ch);
    }

    if (ch->level >= 10)
    {
        sprintf (buf, "Alignment: {W%d{x.\r\nYou are %s.\r\n",
                 ch->alignment, alignment_hr(ch->alignment));
        send_to_char (buf, ch);
    } else {
        sprintf (buf, "You are %s.\r\n", alignment_hr(ch->alignment));
        send_to_char (buf, ch);
    }

}

void do_score (CHAR_DATA * ch, char *argument)
{
    if (argument[0] != 0) {
        if (!str_prefix(argument, "plain")) {
            score_plain(ch);
        } else if (!str_prefix(argument, "tabular")) {
            score_tabular(ch);
        } else {
            score_tabular(ch);
        }
    } else {
        score_tabular(ch);
    }

    if (IS_SET (ch->comm, COMM_SHOW_AFFECTS))
        do_function (ch, &do_affects, "");

}

void do_affects (CHAR_DATA * ch, char *argument)
{
    AFFECT_DATA *paf, *paf_last = NULL;
    char buf[MAX_STRING_LENGTH];

    if (ch->affected != NULL)
    {
        send_to_char ("You are affected by the following spells:\r\n", ch);
        for (paf = ch->affected; paf != NULL; paf = paf->next)
        {
            if (paf_last != NULL && paf->type == paf_last->type)
                if (ch->level >= 20)
                    sprintf (buf, "                      ");
                else
                    continue;
            else
                sprintf (buf, "{mSpell{x: %-15s", skill_table[paf->type].name);

            send_to_char (buf, ch);

            if (ch->level >= 20)
            {
                sprintf (buf,
                         ": modifies %s by %d ",
                         affect_loc_name (paf->location), paf->modifier);
                send_to_char (buf, ch);
                if (paf->duration == -1)
                    sprintf (buf, "permanently");
                else
                    sprintf (buf, "for %d hours", paf->duration);
                send_to_char (buf, ch);
            }

            send_to_char ("\r\n", ch);
            paf_last = paf;
        }
    }
    else
        send_to_char ("You are not affected by any spells.\r\n", ch);

    return;
}



char *const day_name[] = {
    "Mootday", "Bullday", "Midlday", "Thunday", "Freeday",
    "Godday", "Curseday"
};

char *const month_name[] = {
    "Wintrefresh", "the Winter Wolf", "the Frost Giant", "the Old Forces",
    "the Grand Struggle", "the Spring", "Nature", "Futility", "the Dragon",
    "the Sun", "the Heat", "the Battle", "the Dark Shades", "the Shadows",
    "the Long Shadows", "the Ancient Darkness", "the Great Evil"
};

void do_time (CHAR_DATA * ch, char *argument)
{
    extern char str_boot_time[];
    char buf[MAX_STRING_LENGTH];
    char *suf;
    int day;

    day = time_info.day + 1;

    if (day > 4 && day < 20)
        suf = "th";
    else if (day % 10 == 1)
        suf = "st";
    else if (day % 10 == 2)
        suf = "nd";
    else if (day % 10 == 3)
        suf = "rd";
    else
        suf = "th";

    sprintf (buf,
             "It is %d o'clock %s, %s, %d%s of %s.\r\n",
             (time_info.hour % 12 == 0) ? 12 : time_info.hour % 12,
             time_info.hour >= 12 ? "pm" : "am",
             day_name[day % 7], day, suf, month_name[time_info.month]);
    send_to_char (buf, ch);
    sprintf (buf, "ROM started up at %s\r\nThe system time is %s\r\n",
             str_boot_time, (char *) ctime (&current_time));

    send_to_char (buf, ch);
    return;
}



void do_weather (CHAR_DATA * ch, char *argument)
{
    char buf[MAX_STRING_LENGTH];

    static char *const sky_look[4] = {
        "cloudless",
        "cloudy",
        "rainy",
        "lit by flashes of lightning"
    };

    if (!IS_OUTSIDE (ch))
    {
        send_to_char ("You can't see the weather indoors.\r\n", ch);
        return;
    }

    sprintf (buf, "The sky is %s and %s.\r\n",
             sky_look[weather_info.sky],
             weather_info.change >= 0
             ? "a warm southerly breeze blows"
             : "a cold northern gust blows");
    send_to_char (buf, ch);
    return;
}

void do_help (CHAR_DATA * ch, char *argument)
{
    HELP_DATA *pHelp;
    BUFFER *output;
    bool found = FALSE;
    char argall[MAX_INPUT_LENGTH], argone[MAX_INPUT_LENGTH];
    int level;

    output = new_buf ();

    if (argument[0] == '\0')
        argument = "summary";

    /* this parts handles help a b so that it returns help 'a b' */
    argall[0] = '\0';
    while (argument[0] != '\0')
    {
        argument = one_argument (argument, argone);
        if (argall[0] != '\0')
            strcat (argall, " ");
        strcat (argall, argone);
    }

    for (pHelp = help_first; pHelp != NULL; pHelp = pHelp->next)
    {
        level = (pHelp->level < 0) ? -1 * pHelp->level - 1 : pHelp->level;

        if (level > get_trust (ch))
            continue;

        if (is_name (argall, pHelp->keyword))
        {
            /* add seperator if found */
            if (found)
                add_buf (output,
                         "\r\n============================================================\r\n\r\n");
            if (pHelp->level >= 0 && str_cmp (argall, "imotd"))
            {
                add_buf (output, pHelp->keyword);
                add_buf (output, "\r\n");
            }

            /*
             * Strip leading '.' to allow initial blanks.
             */
            if (pHelp->text[0] == '.')
                add_buf (output, pHelp->text + 1);
            else
                add_buf (output, pHelp->text);
            found = TRUE;
            /* small hack :) */
            if (ch->desc != NULL && ch->desc->connected != CON_PLAYING
                && ch->desc->connected != CON_GEN_GROUPS)
                break;
        }
    }

    if (!found)
    {
        send_to_char ("No help on that word.\r\n", ch);
        /*
         * Let's log unmet help requests so studious IMP's can improve their help files ;-)
         * But to avoid idiots, we will check the length of the help request, and trim to
         * a reasonable length (set it by redefining MAX_CMD_LEN in merc.h).  -- JR
         */
        if (strlen(argall) > MAX_CMD_LEN)
        {
            argall[MAX_CMD_LEN - 1] = '\0';
            logit ("Excessive command length: %s requested %s.", ch->name, argall);
            send_to_char ("That was rude!\r\n", ch);
        }
        /* OHELPS_FILE is the "orphaned helps" files. Defined in merc.h -- JR */
        else
        {
            append_file (ch, OHELPS_FILE, argall);
        }
    }
    else
        page_to_char (buf_string (output), ch);
    free_buf (output);
}


/* whois command */
void do_whois (CHAR_DATA * ch, char *argument)
{
    char arg[MAX_INPUT_LENGTH];
    BUFFER *output;
    char buf[MAX_STRING_LENGTH];
    DESCRIPTOR_DATA *d;
    bool found = FALSE;

    one_argument (argument, arg);

    if (arg[0] == '\0')
    {
        send_to_char ("You must provide a name.\r\n", ch);
        return;
    }

    output = new_buf ();

    for (d = descriptor_list; d != NULL; d = d->next)
    {
        CHAR_DATA *wch;
        char const *class;

        if (d->connected != CON_PLAYING || !can_see (ch, d->character))
            continue;

        wch = (d->original != NULL) ? d->original : d->character;

        if (!can_see (ch, wch))
            continue;

        if (!str_prefix (arg, wch->name))
        {
            found = TRUE;

            /* work out the printing */
            class = class_table[wch->class].who_name;
            switch (wch->level)
            {
                case MAX_LEVEL - 0:
                    class = "IMP";
                    break;
                case MAX_LEVEL - 1:
                    class = "CRE";
                    break;
                case MAX_LEVEL - 2:
                    class = "SUP";
                    break;
                case MAX_LEVEL - 3:
                    class = "DEI";
                    break;
                case MAX_LEVEL - 4:
                    class = "GOD";
                    break;
                case MAX_LEVEL - 5:
                    class = "IMM";
                    break;
                case MAX_LEVEL - 6:
                    class = "DEM";
                    break;
                case MAX_LEVEL - 7:
                    class = "ANG";
                    break;
                case MAX_LEVEL - 8:
                    class = "AVA";
                    break;
            }

            /* a little formatting */
            sprintf (buf, "[%2d %6s %s] %s%s%s%s%s%s%s%s\r\n",
                     wch->level,
                     wch->race <
                     MAX_PC_RACE ? pc_race_table[wch->
                                                 race].who_name : "     ",
                     class, wch->incog_level >= LEVEL_HERO ? "({DIncog{x) " : "",
                     wch->invis_level >= LEVEL_HERO ? "(Wizi) " : "",
                     clan_table[wch->clan].who_name, IS_SET (wch->comm,
                                                             COMM_AFK) ?
                     "[AFK] " : "", IS_SET (wch->act,
                                            PLR_KILLER) ? "({RKILLER{x) " : "",
                     IS_SET (wch->act, PLR_THIEF) ? "({BTHIEF{x) " : "",
                     wch->name, IS_NPC (wch) ? "" : wch->pcdata->title);
            add_buf (output, buf);
        }
    }

    if (!found)
    {
        send_to_char ("No one of that name is playing.\r\n", ch);
        return;
    }

    page_to_char (buf_string (output), ch);
    free_buf (output);
}


/*
 * New 'who' command originally by Alander of Rivers of Mud.
 */
void do_who (CHAR_DATA * ch, char *argument)
{
    char buf[MAX_STRING_LENGTH];
    char buf2[MAX_STRING_LENGTH];
    BUFFER *output;
    DESCRIPTOR_DATA *d;
    int iClass;
    int iRace;
    int iClan;
    int iLevelLower;
    int iLevelUpper;
    int nNumber;
    int nMatch;
    bool rgfClass[MAX_CLASS];
    bool rgfRace[MAX_PC_RACE];
    bool rgfClan[MAX_CLAN];
    bool fClassRestrict = FALSE;
    bool fClanRestrict = FALSE;
    bool fClan = FALSE;
    bool fRaceRestrict = FALSE;
    bool fImmortalOnly = FALSE;

    /*
     * Set default arguments.
     */
    iLevelLower = 0;
    iLevelUpper = MAX_LEVEL;
    for (iClass = 0; iClass < MAX_CLASS; iClass++)
        rgfClass[iClass] = FALSE;
    for (iRace = 0; iRace < MAX_PC_RACE; iRace++)
        rgfRace[iRace] = FALSE;
    for (iClan = 0; iClan < MAX_CLAN; iClan++)
        rgfClan[iClan] = FALSE;

    /*
     * Parse arguments.
     */
    nNumber = 0;
    for (;;)
    {
        char arg[MAX_STRING_LENGTH];

        argument = one_argument (argument, arg);
        if (arg[0] == '\0')
            break;

        if (is_number (arg))
        {
            switch (++nNumber)
            {
                case 1:
                    iLevelLower = atoi (arg);
                    break;
                case 2:
                    iLevelUpper = atoi (arg);
                    break;
                default:
                    send_to_char ("Only two level numbers allowed.\r\n", ch);
                    return;
            }
        }
        else
        {

            /*
             * Look for classes to turn on.
             */
            if (!str_prefix (arg, "immortals"))
            {
                fImmortalOnly = TRUE;
            }
            else
            {
                iClass = class_lookup (arg);
                if (iClass == -1)
                {
                    iRace = race_lookup (arg);

                    if (iRace == 0 || iRace >= MAX_PC_RACE)
                    {
                        if (!str_prefix (arg, "clan"))
                            fClan = TRUE;
                        else
                        {
                            iClan = clan_lookup (arg);
                            if (iClan)
                            {
                                fClanRestrict = TRUE;
                                rgfClan[iClan] = TRUE;
                            }
                            else
                            {
                                send_to_char
                                    ("That's not a valid race, class, or clan.\r\n",
                                     ch);
                                return;
                            }
                        }
                    }
                    else
                    {
                        fRaceRestrict = TRUE;
                        rgfRace[iRace] = TRUE;
                    }
                }
                else
                {
                    fClassRestrict = TRUE;
                    rgfClass[iClass] = TRUE;
                }
            }
        }
    }

    /*
     * Now show matching chars.
     */
    nMatch = 0;
    nNumber = 0;
    buf[0] = '\0';
    output = new_buf ();
    for (d = descriptor_list; d != NULL; d = d->next)
    {
        CHAR_DATA *wch;
        char const *class;
        nNumber++;

        /*
         * Check for match against restrictions.
         * Don't use trust as that exposes trusted mortals.
         */
        if (d->connected != CON_PLAYING || !can_see (ch, d->character))
            continue;

        wch = (d->original != NULL) ? d->original : d->character;

        if (!can_see (ch, wch))
            continue;

        if (wch->level < iLevelLower
            || wch->level > iLevelUpper
            || (fImmortalOnly && wch->level < LEVEL_IMMORTAL)
            || (fClassRestrict && !rgfClass[wch->class])
            || (fRaceRestrict && !rgfRace[wch->race])
            || (fClan && !is_clan (wch))
            || (fClanRestrict && !rgfClan[wch->clan]))
            continue;

        nMatch++;

        /*
         * Figure out what to print for class.
         */
        class = class_table[wch->class].who_name;
        switch (wch->level)
        {
            default:
                break;
                {
            case MAX_LEVEL - 0:
                    class = "IMP";
                    break;
            case MAX_LEVEL - 1:
                    class = "CRE";
                    break;
            case MAX_LEVEL - 2:
                    class = "SUP";
                    break;
            case MAX_LEVEL - 3:
                    class = "DEI";
                    break;
            case MAX_LEVEL - 4:
                    class = "GOD";
                    break;
            case MAX_LEVEL - 5:
                    class = "IMM";
                    break;
            case MAX_LEVEL - 6:
                    class = "DEM";
                    break;
            case MAX_LEVEL - 7:
                    class = "ANG";
                    break;
            case MAX_LEVEL - 8:
                    class = "AVA";
                    break;
                }
        }

        /*
         * Format it up.
         */
        sprintf (
            buf, "[{c%2d %6s %s{x] %s%s%s%s%s%s%s%s{x\r\n",
            wch->level,
            wch->race < MAX_PC_RACE ? pc_race_table[wch->race].who_name
            : "     ",
            class,
            wch->incog_level >= LEVEL_HERO ? "({DIncog{x) " : "",
            wch->invis_level >= LEVEL_HERO ? "({CWizi{x) " : "",
            clan_table[wch->clan].who_name,
            IS_SET (wch->comm, COMM_AFK) ? "[{YAFK{x] " : "",
            IS_SET (wch->act, PLR_KILLER) ? "({RKILLER{x) " : "",
            IS_SET (wch->act, PLR_THIEF) ? "({BTHIEF{x) " : "",
            wch->name, IS_NPC (wch) ? "" : wch->pcdata->title);
        add_buf (output, buf);
    }

    sprintf (buf2,
        "\r\nVisible refugees: %d,   overall connections: %d\r\n",
        nMatch, nNumber);
    add_buf (output, buf2);
    page_to_char (buf_string (output), ch);
    free_buf (output);
    return;
}

void do_count (CHAR_DATA * ch, char *argument)
{
    int count;
    DESCRIPTOR_DATA *d;
    char buf[MAX_STRING_LENGTH];

    count = 0;

    for (d = descriptor_list; d != NULL; d = d->next)
        if (d->connected == CON_PLAYING && can_see (ch, d->character))
            count++;

    max_on = UMAX (count, max_on);

    if (max_on == count)
        sprintf (buf,
                 "There are %d characters on, the most so far today.\r\n",
                 count);
    else
        sprintf (buf,
                 "There are %d characters on, the most on today was %d.\r\n",
                 count, max_on);

    send_to_char (buf, ch);
}

void do_inventory (CHAR_DATA * ch, char *argument)
{
    send_to_char ("You are carrying:\r\n", ch);
    show_list_to_char (ch->carrying, ch, TRUE, TRUE);
    return;
}



void do_equipment (CHAR_DATA * ch, char *argument)
{
    OBJ_DATA *obj;
    int iWear;
    bool found;

    send_to_char ("You are using:\r\n", ch);
    found = FALSE;
    for (iWear = 0; iWear < MAX_WEAR; iWear++)
    {
        if ((obj = get_eq_char (ch, iWear)) == NULL)
            continue;

        send_to_char (where_name[iWear], ch);
        if (can_see_obj (ch, obj))
        {
            send_to_char (format_obj_to_char (obj, ch, TRUE), ch);
            send_to_char ("\r\n", ch);
        }
        else
        {
            send_to_char ("something.\r\n", ch);
        }
        found = TRUE;
    }

    if (!found)
        send_to_char ("Nothing.\r\n", ch);

    return;
}



void do_compare (CHAR_DATA * ch, char *argument)
{
    char arg1[MAX_INPUT_LENGTH];
    char arg2[MAX_INPUT_LENGTH];
    OBJ_DATA *obj1;
    OBJ_DATA *obj2;
    int value1;
    int value2;
    char *msg;

    argument = one_argument (argument, arg1);
    argument = one_argument (argument, arg2);
    if (arg1[0] == '\0')
    {
        send_to_char ("Compare what to what?\r\n", ch);
        return;
    }

    if ((obj1 = get_obj_carry (ch, arg1, ch)) == NULL)
    {
        send_to_char ("You do not have that item.\r\n", ch);
        return;
    }

    if (arg2[0] == '\0')
    {
        for (obj2 = ch->carrying; obj2 != NULL; obj2 = obj2->next_content)
        {
            if (obj2->wear_loc != WEAR_NONE && can_see_obj (ch, obj2)
                && obj1->item_type == obj2->item_type
                && (obj1->wear_flags & obj2->wear_flags & ~ITEM_TAKE) != 0)
                break;
        }

        if (obj2 == NULL)
        {
            send_to_char ("You aren't wearing anything comparable.\r\n", ch);
            return;
        }
    }

    else if ((obj2 = get_obj_carry (ch, arg2, ch)) == NULL)
    {
        send_to_char ("You do not have that item.\r\n", ch);
        return;
    }

    msg = NULL;
    value1 = 0;
    value2 = 0;

    if (obj1 == obj2)
    {
        msg = "You compare $p to itself.  It looks about the same.";
    }
    else if (obj1->item_type != obj2->item_type)
    {
        msg = "You can't compare $p and $P.";
    }
    else
    {
        switch (obj1->item_type)
        {
            default:
                msg = "You can't compare $p and $P.";
                break;

            case ITEM_ARMOR:
                value1 = obj1->value[0] + obj1->value[1] + obj1->value[2];
                value2 = obj2->value[0] + obj2->value[1] + obj2->value[2];
                break;

            case ITEM_WEAPON:
                if (obj1->pIndexData->new_format)
                    value1 = (1 + obj1->value[2]) * obj1->value[1];
                else
                    value1 = obj1->value[1] + obj1->value[2];

                if (obj2->pIndexData->new_format)
                    value2 = (1 + obj2->value[2]) * obj2->value[1];
                else
                    value2 = obj2->value[1] + obj2->value[2];
                break;
        }
    }

    if (msg == NULL)
    {
        if (value1 == value2)
            msg = "$p and $P look about the same.";
        else if (value1 > value2)
            msg = "$p looks better than $P.";
        else
            msg = "$p looks worse than $P.";
    }

    act (msg, ch, obj1, obj2, TO_CHAR);
    return;
}



void do_credits (CHAR_DATA * ch, char *argument)
{
    do_function (ch, &do_help, "diku");
    return;
}



void do_where (CHAR_DATA * ch, char *argument)
{
    char buf[MAX_STRING_LENGTH];
    char arg[MAX_INPUT_LENGTH];
    CHAR_DATA *victim;
    DESCRIPTOR_DATA *d;
    bool found;

    one_argument (argument, arg);

    if (arg[0] == '\0')
    {
        send_to_char ("Players near you:\r\n", ch);
        found = FALSE;
        for (d = descriptor_list; d; d = d->next)
        {
            if (d->connected == CON_PLAYING
                && (victim = d->character) != NULL && !IS_NPC (victim)
                && victim->in_room != NULL
                && !IS_SET (victim->in_room->room_flags, ROOM_NOWHERE)
                && (is_room_owner (ch, victim->in_room)
                    || !room_is_private (victim->in_room))
                && victim->in_room->area == ch->in_room->area
                && can_see (ch, victim))
            {
                found = TRUE;
                sprintf (buf, "%-28s %s\r\n",
                         victim->name, victim->in_room->name);
                send_to_char (buf, ch);
            }
        }
        if (!found)
            send_to_char ("None\r\n", ch);
    }
    else
    {
        found = FALSE;
        for (victim = char_list; victim != NULL; victim = victim->next)
        {
            if (victim->in_room != NULL
                && victim->in_room->area == ch->in_room->area
                && !IS_AFFECTED (victim, AFF_HIDE)
                && !IS_AFFECTED (victim, AFF_SNEAK)
                && can_see (ch, victim) && is_name (arg, victim->name))
            {
                found = TRUE;
                sprintf (buf, "%-28s %s\r\n",
                         PERS (victim, ch), victim->in_room->name);
                send_to_char (buf, ch);
                break;
            }
        }
        if (!found)
            act ("You didn't find any $T.", ch, NULL, arg, TO_CHAR);
    }

    return;
}




void do_consider (CHAR_DATA * ch, char *argument)
{
    char arg[MAX_INPUT_LENGTH];
    CHAR_DATA *victim;
    char *msg;
    int diff;

    one_argument (argument, arg);

    if (arg[0] == '\0')
    {
        send_to_char ("Consider killing whom?\r\n", ch);
        return;
    }

    if ((victim = get_char_room (ch, arg)) == NULL)
    {
        send_to_char ("They're not here.\r\n", ch);
        return;
    }

    if (is_safe (ch, victim))
    {
        send_to_char ("Don't even think about it.\r\n", ch);
        return;
    }

    diff = victim->level - ch->level;

    if (diff <= -10)
        msg = "You can kill $N naked and weaponless.";
    else if (diff <= -5)
        msg = "$N is no match for you.";
    else if (diff <= -2)
        msg = "$N looks like an easy kill.";
    else if (diff <= 1)
        msg = "The perfect match!";
    else if (diff <= 4)
        msg = "$N says 'Do you feel lucky, punk?'.";
    else if (diff <= 9)
        msg = "$N laughs at you mercilessly.";
    else
        msg = "Death will thank you for your gift.";

    act (msg, ch, NULL, victim, TO_CHAR);
    return;
}



void set_title (CHAR_DATA * ch, char *title)
{
    char buf[MAX_STRING_LENGTH];

    if (IS_NPC (ch))
    {
        bug ("Set_title: NPC.", 0);
        return;
    }

    if (title[0] != '.' && title[0] != ',' && title[0] != '!'
        && title[0] != '?')
    {
        buf[0] = ' ';
        strcpy (buf + 1, title);
    }
    else
    {
        strcpy (buf, title);
    }

    free_string (ch->pcdata->title);
    ch->pcdata->title = str_dup (buf);
    return;
}



void do_title (CHAR_DATA * ch, char *argument)
{
    int i;

    if (IS_NPC (ch))
        return;

    /* Changed this around a bit to do some sanitization first   *
     * before checking length of the title. Need to come up with *
     * a centralized user input sanitization scheme. FIXME!      *
     * JR -- 10/15/00                                            */

    if (strlen (argument) > 45)
        argument[45] = '\0';

    i = strlen(argument);
    if (argument[i-1] == '{' && argument[i-2] != '{')
        argument[i-1] = '\0';

    if (argument[0] == '\0')
    {
        send_to_char ("Change your title to what?\r\n", ch);
        return;
    }

    smash_tilde (argument);
    smash_newline (argument);
    set_title (ch, argument);
    send_to_char ("Ok.\r\n", ch);
}

void do_description (CHAR_DATA * ch, char *argument)
{
    char buf[MAX_STRING_LENGTH];

    if (argument[0] != '\0')
    {
        buf[0] = '\0';
        smash_tilde (argument);

        if (argument[0] == '-')
        {
            int len;
            bool found = FALSE;

            if (ch->description == NULL || ch->description[0] == '\0')
            {
                send_to_char ("No lines left to remove.\r\n", ch);
                return;
            }

            strcpy (buf, ch->description);

            for (len = strlen (buf); len > 0; len--)
            {
                if (buf[len] == '\r')
                {
                    if (!found)
                    {            /* back it up */
                        if (len > 0)
                            len--;
                        found = TRUE;
                    }
                    else
                    {            /* found the second one */

                        buf[len + 1] = '\0';
                        free_string (ch->description);
                        ch->description = str_dup (buf);
                        send_to_char ("Your description is:\r\n", ch);
                        send_to_char (ch->description ? ch->description :
                                      "(None).\r\n", ch);
                        return;
                    }
                }
            }
            buf[0] = '\0';
            free_string (ch->description);
            ch->description = str_dup (buf);
            send_to_char ("Description cleared.\r\n", ch);
            return;
        }
        if (argument[0] == '+')
        {
            if (ch->description != NULL)
                strcat (buf, ch->description);
            argument++;
            while (isspace (*argument))
                argument++;
        }

        if (strlen (buf) >= 1024)
        {
            send_to_char ("Description too long.\r\n", ch);
            return;
        }

        strcat (buf, argument);
        strcat (buf, "\r\n");
        free_string (ch->description);
        ch->description = str_dup (buf);
    }

    send_to_char ("Your description is:\r\n", ch);
    send_to_char (ch->description ? ch->description : "(None).\r\n", ch);
    return;
}



void do_report (CHAR_DATA * ch, char *argument)
{
    char buf[MAX_INPUT_LENGTH];

    sprintf (buf,
             "{gYou say '{GI have %d/%d hp %d/%d mana %d/%d mv %d xp.{g'{x\r\n",
             ch->hit, ch->max_hit,
             ch->mana, ch->max_mana, ch->move, ch->max_move, ch->exp);

    send_to_char (buf, ch);

    sprintf (buf, "{g$n says '{GI have %d/%d hp %d/%d mana %d/%d mv %d xp.{g'{x",
             ch->hit, ch->max_hit,
             ch->mana, ch->max_mana, ch->move, ch->max_move, ch->exp);

    act (buf, ch, NULL, NULL, TO_ROOM);

    return;
}



void do_practice (CHAR_DATA * ch, char *argument)
{
    char buf[MAX_STRING_LENGTH];
    int sn;

    if (IS_NPC (ch))
        return;

    if (argument[0] == '\0')
    {
        int col;

        col = 0;
        for (sn = 0; sn < MAX_SKILL; sn++)
        {
            if (skill_table[sn].name == NULL)
                break;
            if (ch->level < skill_table[sn].skill_level[ch->class]
                || ch->pcdata->learned[sn] < 1 /* skill is not known */ )
                continue;

            sprintf (buf, "%-18s %3d%%  ",
                     skill_table[sn].name, ch->pcdata->learned[sn]);
            send_to_char (buf, ch);
            if (++col % 3 == 0)
                send_to_char ("\r\n", ch);
        }

        if (col % 3 != 0)
            send_to_char ("\r\n", ch);

        sprintf (buf, "You have %d practice sessions left.\r\n",
                 ch->practice);
        send_to_char (buf, ch);
    }
    else
    {
        CHAR_DATA *mob;
        int adept;

        if (!IS_AWAKE (ch))
        {
            send_to_char ("In your dreams, or what?\r\n", ch);
            return;
        }

        for (mob = ch->in_room->people; mob != NULL; mob = mob->next_in_room)
        {
            if (IS_NPC (mob) && IS_SET (mob->act, ACT_PRACTICE))
                break;
        }

        if (mob == NULL)
        {
            send_to_char ("You can't do that here.\r\n", ch);
            return;
        }

        if (ch->practice <= 0)
        {
            send_to_char ("You have no practice sessions left.\r\n", ch);
            return;
        }

        if ((sn = find_spell (ch, argument)) < 0 ||
            (!IS_NPC (ch)
                && (ch->level < skill_table[sn].skill_level[ch->class]
                    || ch->pcdata->learned[sn] < 1    /* skill is not known */
                    || skill_table[sn].rating[ch->class] == 0)))
        {
            send_to_char ("You can't practice that.\r\n", ch);
            return;
        }

        adept = IS_NPC (ch) ? 100 : class_table[ch->class].skill_adept;

        if (ch->pcdata->learned[sn] >= adept)
        {
            sprintf (buf, "You are already learned at %s.\r\n",
                     skill_table[sn].name);
            send_to_char (buf, ch);
        }
        else
        {
            ch->practice--;
            ch->pcdata->learned[sn] +=
                int_app[get_curr_stat (ch, STAT_INT)].learn /
                skill_table[sn].rating[ch->class];
            if (ch->pcdata->learned[sn] < adept)
            {
                sprintf (buf, "You practice %s to %d%%.\r\n",
                    skill_table[sn].name, ch->pcdata->learned[sn]);
                send_to_char (buf, ch);
                act ("$n practices $T.",
                     ch, NULL, skill_table[sn].name, TO_ROOM);
            }
            else
            {
                ch->pcdata->learned[sn] = adept;
                act ("You are now learned at $T.",
                     ch, NULL, skill_table[sn].name, TO_CHAR);
                act ("$n is now learned at $T.",
                     ch, NULL, skill_table[sn].name, TO_ROOM);
            }
        }
    }
    return;
}



/*
 * 'Wimpy' originally by Dionysos.
 */
void do_wimpy (CHAR_DATA * ch, char *argument)
{
    char buf[MAX_STRING_LENGTH];
    char arg[MAX_INPUT_LENGTH];
    int wimpy;

    one_argument (argument, arg);

    if (arg[0] == '\0')
        wimpy = ch->max_hit / 5;
    else
        wimpy = atoi (arg);

    if (wimpy < 0)
    {
        send_to_char ("Your courage exceeds your wisdom.\r\n", ch);
        return;
    }

    if (wimpy > ch->max_hit / 2)
    {
        send_to_char ("Such cowardice ill becomes you.\r\n", ch);
        return;
    }

    ch->wimpy = wimpy;
    sprintf (buf, "Wimpy set to %d hit points.\r\n", wimpy);
    send_to_char (buf, ch);
    return;
}



void do_password (CHAR_DATA * ch, char *argument)
{
    char arg1[MAX_INPUT_LENGTH];
    char arg2[MAX_INPUT_LENGTH];
    char *pArg;
    char hash_string[62];
    char cEnd;

    if (IS_NPC (ch))
        return;

    /*
     * Can't use one_argument here because it smashes case.
     * So we just steal all its code.  Bleagh.
     */
    pArg = arg1;
    while (isspace (*argument))
        argument++;

    cEnd = ' ';
    if (*argument == '\'' || *argument == '"')
        cEnd = *argument++;

    while (*argument != '\0')
    {
        if (*argument == cEnd)
        {
            argument++;
            break;
        }
        *pArg++ = *argument++;
    }
    *pArg = '\0';

    pArg = arg2;
    while (isspace (*argument))
        argument++;

    cEnd = ' ';
    if (*argument == '\'' || *argument == '"')
        cEnd = *argument++;

    while (*argument != '\0')
    {
        if (*argument == cEnd)
        {
            argument++;
            break;
        }
        *pArg++ = *argument++;
    }
    *pArg = '\0';

    if (arg1[0] == '\0' || arg2[0] == '\0')
    {
        send_to_char ("Syntax: password <old> <new>.\r\n", ch);
        return;
    }

    if (check_passwd(arg1, ch->pcdata->pwd))
    {
        WAIT_STATE (ch, 40);
        send_to_char ("Wrong password.  Wait 10 seconds.\r\n", ch);
        return;
    }

    if (strlen (arg2) < 5)
    {
        send_to_char
            ("New password must be at least five characters long.\r\n", ch);
        return;
    }

    /*
     * No tilde allowed because of player file format.
     */
    create_passwd(arg2, hash_string);

    free_string (ch->pcdata->pwd);
    ch->pcdata->pwd = str_dup (hash_string);
    save_char_obj (ch);
    send_to_char ("Ok.\r\n", ch);
    return;
}

void do_telnetga (CHAR_DATA * ch, char *argument)
{
    if (IS_NPC (ch))
        return;

    if (IS_SET (ch->comm, COMM_TELNET_GA))
    {
        send_to_char ("Telnet GA removed.\r\n", ch);
        REMOVE_BIT (ch->comm, COMM_TELNET_GA);
    }
    else
    {
        send_to_char ("Telnet GA enabled.\r\n", ch);
        SET_BIT (ch->comm, COMM_TELNET_GA);
    }
}
