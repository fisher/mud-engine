/*
 * version.c
 *
 * Copyright 2017 Serge Ribalchenko <fisher@heim.in.ua>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include "version.h"

/*
 * The main idea is to use the only one .c file that will be
 * recompiled each time you run make, providing fresh version
 * info through dynimic function calls to its API
 */
inline char *release()
{
    return RELEASE;
}

inline char *version() { return VERSION; }

inline char *build_time()
{
    return BLD_TIME;
}

inline char *build_hostname()
{
    return BLD_HOSTNAME;
}

inline char *build_kernel()
{
    return BLD_KERNEL;
}

inline char *build_kernel_release()
{
    return BLD_KERN_REL;
}
